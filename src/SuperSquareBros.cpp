#include "SuperSquareBros.hpp"

#include "Camera.hpp"
#include "GameInput.hpp"
#include "Constants.hpp"
#include "Effects.hpp"
#include "LevelObjects.hpp"
#include "entities/Entity.hpp"
#include "scenes/Scene.h"
#include "Data.hpp"
#include "GameState.hpp"

using namespace blit;

// Note: if want to go hires, with lores graphics:
// screen.sprite(uint16_t sprite, const Point &pos, const Point &origin, float scale, uint8_t transform)
// origin can be Point(0,0) and transform can be SpriteTransform::NONE, scale is 2

// e.g. screen.sprite(id, Point(x, y), Point(0, 0), 2.0f, SpriteTransform::NONE

#define RESET_SAVE_DATA_IF_MINOR_DIFF
//#define TESTING_MODE

const std::vector<uint16_t> transitionFramesClose = { TILE_ID_TRANSITION, TILE_ID_TRANSITION + 1, TILE_ID_TRANSITION + 2, TILE_ID_TRANSITION + 3, TILE_ID_TRANSITION + 4, TILE_ID_TRANSITION + 6, TILE_ID_TRANSITION + 7};
const std::vector<uint16_t> transitionFramesOpen = { TILE_ID_TRANSITION + 6, TILE_ID_TRANSITION + 5, TILE_ID_TRANSITION + 4, TILE_ID_TRANSITION + 3, TILE_ID_TRANSITION + 2, TILE_ID_TRANSITION + 1, TILE_ID_TRANSITION};


///////////////////////////////////////////////////////////////////////////
//
// init()
//
// setup your game here
//
void init() {
    set_screen_mode(ScreenMode::lores);

    screen.sprites = Surface::load(asset_sprites);

    screen.alpha = 255;
    screen.mask = nullptr;

    // Load metadata
    gameState.metadata = get_metadata();
    // parse version
    {
        std::string versionString = gameState.metadata.version;

        GameVersion version{};

        uint8_t startIndex, endIndex;

        startIndex = 1;
        endIndex = versionString.find('.');

        version.major = (uint8_t)std::stoi(versionString.substr(startIndex, endIndex));

        startIndex = endIndex + 1;
        endIndex = versionString.find('.', startIndex);

        version.minor = (uint8_t)std::stoi(versionString.substr(startIndex, endIndex));

        startIndex = endIndex + 1;
        endIndex = versionString.size();

        version.build = (uint8_t)std::stoi(versionString.substr(startIndex, endIndex));

        gameState.gameVersion = version;
    }
    printf("Loaded metadata. Game version: %d (v%d.%d.%d)\n", get_version(gameState.gameVersion), gameState.gameVersion.major,
           gameState.gameVersion.minor, gameState.gameVersion.build);

    // Populate transition array
    for (uint8_t y = 0; y < SCREEN_HEIGHT / SPRITE_SIZE; y++) {
        for (uint8_t x = 0; x < SCREEN_WIDTH / SPRITE_SIZE; x++) {
            gameState.transition[y * (SCREEN_WIDTH / SPRITE_SIZE) + x] = AnimatedTransition(x * SPRITE_SIZE, y * SPRITE_SIZE,
                                                                                              transitionFramesOpen,
                                                                                              transitionFramesClose);
        }
    }

    gameState.allPlayerSaveData[0] = load_player_data(0);
    gameState.allPlayerSaveData[1] = load_player_data(1);

    // Load level data
    for (uint8_t i = 0; i < LEVEL_COUNT; i++) {
        gameState.allLevelSaveData[0][i] = load_level_data(0, i);
    }
    for (uint8_t i = 0; i < LEVEL_COUNT; i++) {
        gameState.allLevelSaveData[1][i] = load_level_data(1, i);
    }

    // load audio
    {
        gameState.audioHandler.init();

#ifndef PICO_BUILD
// NOTE: CURRENTLY ISSUE WITH LEAVING PAUSE MENU, blip AUDIO IS PLAYED, BUT THEN NEW SOUND IS LOADED IN, STOPPING PLAYBACK.

        // Setup audio volumes
        gameState.audioHandler.set_volume(gameState.gameSaveData.sfxVolume ? DEFAULT_VOLUME : 0);
        gameState.audioHandler.set_volume(7, gameState.gameSaveData.musicVolume ? DEFAULT_VOLUME : 0);

        // Sfx
        gameState.audioHandler.load(0, asset_sound_select, asset_sound_select_length);
        gameState.audioHandler.load(1, asset_sound_jump, asset_sound_jump_length);
        gameState.audioHandler.load(2, asset_sound_coin, asset_sound_coin_length);
        gameState.audioHandler.load(3, asset_sound_enemydeath, asset_sound_enemydeath_length);
        gameState.audioHandler.load(4, asset_sound_enemyinjured, asset_sound_enemyinjured_length);
        gameState.audioHandler.load(5, asset_sound_playerdeath, asset_sound_playerdeath_length);
        gameState.audioHandler.load(6, asset_sound_enemythrow, asset_sound_enemythrow_length);
        // Music
        gameState.audioHandler.load(7, asset_music_splash, asset_music_splash_length);

        // Note: to play sfx0, call audioHandler.play(0)
// For music, need to load sound when changing (i.e. audioHandler.load(7, asset_music_<music>, asset_music_<music>_length); audioHandler.play(7, 0b11);
#endif // PICO_BUILD

        // Start splash music playing
        gameState.audioHandler.play(7);
    }
}

///////////////////////////////////////////////////////////////////////////
//
// render(time)
//
// This function is called to perform rendering of the game. time is the 
// amount if milliseconds elapsed since the start of your game
//
void render(uint32_t time) {

    gameState.currentScene->render();

    // render transition
    for (uint16_t i1 = 0; i1 < SCREEN_TILE_SIZE; i1++) {
        gameState.transition[i1].render(gameState.camera);
    }

    if (gameState.splashColour.a != 0) {
        screen.pen = Pen(gameState.splashColour.r, gameState.splashColour.g, gameState.splashColour.b, gameState.splashColour.a);
        screen.clear();
    }
}

///////////////////////////////////////////////////////////////////////////
//
// update(time)
//
// This is called to update your game state. time is the 
// amount if milliseconds elapsed since the start of your game
//
void update(uint32_t time) {
    // Get dt
    gameState.dt = (time - gameState.lastTime) / 1000.0;
    gameState.lastTime = time;

    // Cap dt
    if (gameState.dt > 0.05f) {
        gameState.dt = 0.05f;
    }

    gameState.textFlashTimer += gameState.dt;
    if (gameState.textFlashTimer >= TEXT_FLASH_TIME) {
        gameState.textFlashTimer -= TEXT_FLASH_TIME;
    }

    // Update buttonStates
    gameState.buttonStates.update();

    // Update game
    gameState.currentScene->update();

    // update transition
    {
        ButtonStates &buttonStates1 = gameState.buttonStates;
        for (uint16_t i = 0; i < SCREEN_TILE_SIZE; i++) {
            gameState.transition[i].update(gameState.dt, buttonStates1);
        }
    }

    // Screen shake
    gameState.camera.pos.x += gameState.shaker.time_to_shake(gameState.dt);
    gameState.camera.pos.y += gameState.shaker.time_to_shake(gameState.dt);

    gameState.audioHandler.update(gameState.dt);
}
