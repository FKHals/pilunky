#ifndef SUPER_SQUARE_BROS_LEVEL_H
#define SUPER_SQUARE_BROS_LEVEL_H

#include "Camera.hpp"
#include "Constants.hpp"
#include "Effects.hpp"
#include "GameInput.hpp"
#include "Point_u16.hpp"


const uint16_t TILE_ID_EMPTY = 255;

const uint16_t TILE_ID_COIN = 384;
const uint16_t TILE_ID_PLAYER_1 = 192;
const uint16_t TILE_ID_PLAYER_2 = 196;
const uint16_t TILE_ID_HEART = 416;
const uint16_t TILE_ID_CAMERA = 509;
const uint16_t TILE_ID_TRANSITION = 496;
const uint16_t TILE_ID_FINISH = 432;
const uint16_t TILE_ID_CHECKPOINT = 404;
const uint16_t TILE_ID_LEVEL_TRIGGER = 420;
const uint16_t TILE_ID_LEVEL_BRIDGE_MIN = 144;

const uint16_t TILE_ID_ENEMY_1 = 208;
const uint16_t TILE_ID_ENEMY_2 = 212;
const uint16_t TILE_ID_ENEMY_3 = 216;
const uint16_t TILE_ID_ENEMY_4 = 220;
const uint16_t TILE_ID_ENEMY_5 = 224;
const uint16_t TILE_ID_ENEMY_6 = 228;
const uint16_t TILE_ID_ENEMY_7 = 232;
const uint16_t TILE_ID_ENEMY_8 = 236;
const uint16_t TILE_ID_ENEMY_9 = 240;

const uint16_t TILE_ID_BOSS_1 = 256;
const uint16_t TILE_ID_BOSS_2 = 264;
const uint16_t TILE_ID_BIG_BOSS = 288;

const uint16_t TILE_ID_SPIKE_BOTTOM = 480;
const uint16_t TILE_ID_SPIKE_TOP = 481;
const uint16_t TILE_ID_SPIKE_LEFT = 482;
const uint16_t TILE_ID_SPIKE_RIGHT = 483;

const uint16_t TILE_ID_ENEMY_PROJECTILE_ROCK = 472;
const uint16_t TILE_ID_ENEMY_PROJECTILE_SNOWBALL = 473;
const uint16_t TILE_ID_ENEMY_PROJECTILE_BULLET = 474;

const uint16_t TILE_ID_BOSS_PROJECTILE_ROCK = 476;
const uint16_t TILE_ID_BOSS_PROJECTILE_SNOWBALL = 477;

const uint16_t TILE_ID_HUD_LIVES = 422;
const uint16_t TILE_ID_HUD_COINS = 424;
const uint16_t TILE_ID_HUD_ENEMIES_KILLED = 425;
const uint16_t TILE_ID_HUD_TIME_TAKEN = 426;

const uint16_t TILE_ID_GOLD_BADGE = 428;

class LevelObject {
public:
    /// pos
    Point_u16 pos;

    LevelObject();
    LevelObject(uint16_t xPosition, uint16_t yPosition);

    virtual void update(float dt, ButtonStates& buttonStates) = 0;
    virtual void render(Camera& camera) = 0;
};

class Pickup : public LevelObject {
public:
    bool collected;

    Pickup();
    Pickup(uint16_t xPosition, uint16_t yPosition);

    virtual void update(float dt, ButtonStates& buttonStates) = 0;
    virtual void render(Camera& camera) = 0;
};

class AnimatedPickup : public Pickup {
public:
    AnimatedPickup();
    AnimatedPickup(uint16_t xPosition, uint16_t yPosition,
                   std::vector<uint16_t> animationFrames);
    void update(float dt, ButtonStates& buttonStates) override;
    void render(Camera& camera) override;

protected:
    float animationTimer;
    std::vector<uint16_t> frames;
    uint16_t currentFrame;
};

class Coin : public AnimatedPickup {
public:
    Coin();
    Coin(uint16_t xPosition, uint16_t yPosition,
         std::vector<uint16_t> animationFrames);
    void update(float dt, ButtonStates& buttonStates) final;
    void render(Camera& camera) final;
};

class Finish : public AnimatedPickup {
public:
    std::vector<Particle> particles;
    float particleTimer;

    Finish();
    Finish(uint16_t xPosition, uint16_t yPosition, std::vector<uint16_t> animationFrames);
    void update(float dt, ButtonStates& buttonStates) final;
    void render(Camera& camera) final;
};

class Checkpoint : public LevelObject {
public:
    std::vector<Particle> particles;
    uint8_t colour;
    bool generateParticles;

    Checkpoint();
    Checkpoint(uint16_t xPosition, uint16_t yPosition);
    void update(float dt, ButtonStates& buttonStates);
    void render(Camera& camera);
    bool activate(uint8_t c);

protected:
    float animationTimer;
    uint16_t currentFrame;
};

class LevelTrigger : public LevelObject {
public:
    std::vector<Particle> particles;
    uint8_t levelNumber;
    bool visible;
    bool generateParticles;

    LevelTrigger();
    LevelTrigger(uint16_t xPosition, uint16_t yPosition, uint8_t levelTriggerNumber);
    void update(float dt, ButtonStates& buttonStates);
    void render(Camera& camera);
    void set_active();

protected:
    float textY;
    float textVelY;
};

#endif //SUPER_SQUARE_BROS_LEVEL_H
