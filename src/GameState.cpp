#include "GameState.hpp"

#include "LevelObjects.hpp"

/**
 * This is the GLOBAL (!) game state. This is somewhat ugly but most probably faster than passing a reference to a
 * GameState around and the Picosystem seems to be really resource-limited.
 */

GameState gameState;

GameState::GameState() {
    lastTime = 0;
    menuBack = false; // tells menu to go backwards instead of forwards.
    gamePaused = false; // used for determining if game is paused or not.

    coinSfxAlternator = false; // used for alternating channel used for coin pickup sfx

    cameraIntro = false;
    cameraRespawn = false;
    cameraNewWorld = false;

    textFlashTimer = 0.0f;
    playerSelected = 0;
    pauseMenuItem = 0;
    menuItem = 0;
    settingsItem = 0;
    creditsItem = 0;

    snowGenTimer = 0.0f;
    confettiGenTimer = 0.0f;

    slowPlayer = false;
    dropPlayer = false;
    repelPlayer = false;
    bossBattle = false;

    thankyouValue = 0.0f;

    currentLevelNumber = NO_LEVEL_SELECTED;
    currentWorldNumber = 0;

    currentScene = &scene::sgIcon;

    buttonStates = {0};

    shaker = ScreenShake(SCREEN_SHAKE_SHAKINESS);

    splashColour = Colour(7, 0, 14, 0);
}

Tile GameState::getMapTile(uint16_t x_pos, uint16_t y_pos) {
    if ((0 <= x_pos && x_pos < MAX_MAP_WIDTH)
        && (0 <= y_pos && y_pos < MAX_MAP_HEIGHT)) {
        return map[y_pos][x_pos];
    } else {
        // outside the map there is only emptiness
        return Tile::EMPTY;
    }
}

void GameState::setMapTile(uint16_t x_pos, uint16_t y_pos, Tile tile) {
    if ((0 <= x_pos && x_pos < MAX_MAP_WIDTH)
        && (0 <= y_pos && y_pos < MAX_MAP_HEIGHT)) {
        map[y_pos][x_pos] = tile;
    }
}

void switchSceneTo(Scene const *nextScene) {
    gameState.currentScene = nextScene;
    nextScene->onSwitch();
}
