#ifndef SUPER_SQUARE_BROS_POINT16_H
#define SUPER_SQUARE_BROS_POINT16_H

#include <cstdint>

#include "engine/engine.hpp"

struct Point_u16 {
    uint16_t x = 0, y = 0;

    Point_u16() = default;
    Point_u16(const Point_u16 &p) = default;
    constexpr Point_u16(uint16_t x, uint16_t y) : x(x), y(y) {}
    constexpr Point_u16(blit::Vec2 v) : x(uint16_t(v.x)), y(uint16_t(v.y)) {}

    inline Point_u16& operator-= (const Point_u16 &a) { x -= a.x; y -= a.y; return *this; }
    inline Point_u16& operator+= (const Point_u16 &a) { x += a.x; y += a.y; return *this; }
    inline Point_u16& operator*= (const float a) { x = static_cast<uint16_t>(x * a); y = static_cast<uint16_t>(y * a); return *this; }
    inline Point_u16& operator/= (const uint16_t a) { x /= a; y /= a;   return *this; }
};

inline bool operator== (const Point_u16 &lhs, const Point_u16 &rhs) { return lhs.x == rhs.x && lhs.y == rhs.y; }
inline bool operator!= (const Point_u16 &lhs, const Point_u16 &rhs) { return !(lhs == rhs); }
inline Point_u16 operator-  (Point_u16 lhs, const Point_u16 &rhs) { lhs -= rhs; return lhs; }
inline Point_u16 operator+  (Point_u16 lhs, const Point_u16 &rhs) { lhs += rhs; return lhs; }
inline Point_u16 operator*  (Point_u16 lhs, const float a) { lhs *= a; return lhs; }
inline Point_u16 operator/  (Point_u16 lhs, const uint16_t a) { lhs /= a; return lhs; }

#endif //SUPER_SQUARE_BROS_POINT16_H
