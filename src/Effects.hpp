#ifndef SUPER_SQUARE_BROS_EFFECTS_H
#define SUPER_SQUARE_BROS_EFFECTS_H

#include "engine/api.hpp"
#include "Camera.hpp"
#include "Constants.hpp"


class Colour {
public:
    uint8_t r, g, b, a;

    Colour();
    Colour(uint8_t r, uint8_t g, uint8_t b);
    Colour(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
};

// Particle colours
const std::array<Colour, 3> playerDeathParticleColours[2] = {
        { Colour(255, 255, 242), Colour(255, 204, 181), Colour(178, 53, 53) },
        { Colour(255, 255, 242), Colour(178, 214, 96), Colour(37, 124, 73) }
};
const std::array<Colour, 3> enemyDeathParticleColours[5] = {
        { Colour(255, 255, 242), Colour(184, 197, 216), Colour(25, 40, 102) },
        { Colour(255, 255, 242), Colour(255, 204, 181), Colour(165, 82, 139) },
        { Colour(255, 255, 242), Colour(255, 204, 181), Colour(229, 114, 57) },
        { Colour(255, 255, 242), Colour(204, 137, 124), Colour(127, 24, 75) },
        { Colour(255, 255, 242), Colour(145, 224, 204), Colour(53, 130, 130) }
};
const std::array<Colour, 3> bossDeathParticleColours[3] = {
        { Colour(255, 255, 242), Colour(184, 197, 216), Colour(25, 40, 102) },
        { Colour(255, 255, 242), Colour(255, 204, 181), Colour(165, 82, 139) },
        { Colour(255, 255, 242), Colour(184, 197, 216), Colour(25, 40, 102) }
};
const std::array<Colour, 3> levelTriggerParticleColours = { Colour(255, 255, 242), Colour(145, 224, 204), Colour(53, 130, 130) };

const std::array<Colour, 3> checkpointParticleColours[3] = {
        { Colour(255, 255, 242), Colour(184, 197, 216) },
        { Colour(178, 53, 53), Colour(127, 24, 75) },
        { Colour(37, 124, 73), Colour(16, 84, 72) }
};

const std::array<Colour, 3> finishParticleColours = { Colour(37, 124, 73), Colour(16, 84, 72), Colour(10, 57, 71) };

const std::array<Colour, 3> slowPlayerParticleColours = { Colour(145, 224, 204), Colour(53, 130, 130) };//Colour(255, 255, 242),
const std::array<Colour, 3> repelPlayerParticleColours = { Colour(255, 235, 140), Colour(255, 199, 89) };

const Colour inputSelectColour = Colour(255, 199, 89);
const Colour hudBackground = Colour(7, 0, 14, 64);
const Colour gameBackground = Colour(62, 106, 178);
const Colour defaultWhite = Colour(255, 255, 242);
const Colour niceBlue = Colour(0x91, 0xE0, 0xCC);


class Particle {
public:
    blit::Vec2 pos;
    blit::Vec2 vel;
    blit::Vec2 gravity;
    Colour colour;
    float age;

    Particle();
    Particle(float xPosition, float yPosition,
             float xVelocity, float yVelocity,
             float particleGravityX, float particleGravityY,
             Colour particleColour);
    void render(Camera& camera);
    void update(float dt);
};

class ImageParticle {
public:
    blit::Vec2 pos;
    blit::Vec2 vel;
    blit::Vec2 gravity;
    uint16_t id;

    ImageParticle();
    ImageParticle(float xPosition, float yPosition, float xVelocity, float yVelocity, float particleGravityX, float particleGravityY, uint16_t tileID);
    void render(Camera& camera);
    void update(float dt);
};

class BrownianParticle : public Particle {
public:
    BrownianParticle();
    BrownianParticle(float xPosition, float yPosition,
                     uint16_t angle, float speed,
                     float particleGravityX, float particleGravityY,
                     Colour particleColour, uint8_t angleWiggle);
    void update(float dt);

protected:
    uint8_t angleWiggle;
    uint16_t angle;
    float speed;
};

std::vector<Particle> generate_particles(float x, float y,
                                         float gravityX, float gravityY,
                                         std::array<Colour, 3> colours, float speed, uint8_t count);
BrownianParticle generate_brownian_particle(float x, float y,
                                            float gravityX, float gravityY,
                                            float speed, std::array<Colour, 3> colours, uint8_t wiggle);

class AnimatedTransition {
public:
    uint8_t x, y;
    float closedTimer;

    AnimatedTransition();
    AnimatedTransition(uint16_t xPosition, uint16_t yPosition,
                       std::vector<uint16_t> open, std::vector<uint16_t> close);
    void update(float dt, ButtonStates& buttonStates);
    void render(Camera& camera);
    void close();
    void open();
    bool is_closed();
    bool is_ready_to_open();
    bool is_open();

protected:
    enum class TransitionState {
        OPENING,
        OPEN,
        CLOSING,
        CLOSED,
        READY_TO_OPEN
    } state;

    float animationTimer;
    std::vector<uint16_t> openingFrames, closingFrames;
    uint16_t currentFrame;
};

#endif //SUPER_SQUARE_BROS_EFFECTS_H
