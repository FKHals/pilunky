#ifndef SUPER_SQUARE_BROS_TILES_H
#define SUPER_SQUARE_BROS_TILES_H

#include <cstdint>


struct Tile {
    uint8_t id;
    uint8_t flags;
    uint16_t sprite;

    static const uint8_t NONE = 0x0;
    static const uint8_t COLLIDABLE = 0x01;
    //const uint8_t LOS_BLOCKING = 0x02;
    //const uint8_t FLAMMABLE = 0x04;
    //const uint8_t SECRET = 0x08;
    //const uint8_t SOLID = 0x10;
    //const uint8_t AVOID = 0x20;
    //const uint8_t LIQUID = 0x40;
    //const uint8_t PIT = 0x80;

    [[nodiscard]] bool isCollidable() const {
        return (flags & COLLIDABLE) != 0;
    }

    static const Tile EMPTY;
    static const Tile SOLID;
};

#endif //SUPER_SQUARE_BROS_TILES_H
