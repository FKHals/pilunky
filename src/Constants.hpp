#ifndef SUPER_SQUARE_BROS_CONSTANTS_H
#define SUPER_SQUARE_BROS_CONSTANTS_H

#include "Camera.hpp"
#include "GameInput.hpp"

/**
 * This file includes generally needed stuff that i could not yet figure out where to put in the structure
 */
// TODO: Remove this file if a better name or structure is found!

#ifdef PICO_BUILD
const uint16_t SCREEN_WIDTH = 120;
const uint16_t SCREEN_HEIGHT = 120;

#else
const uint16_t SCREEN_WIDTH = 160;
const uint16_t SCREEN_HEIGHT = 120;
#endif // PICO_BUILD

const uint16_t SCREEN_MID_WIDTH = SCREEN_WIDTH / 2;
const uint16_t SCREEN_MID_HEIGHT = SCREEN_HEIGHT / 2;

const uint8_t SPRITE_SIZE = 8;
const uint8_t SPRITE_HALF = SPRITE_SIZE / 2;
const uint8_t SPRITE_QUARTER = SPRITE_SIZE / 4;

const uint16_t SCREEN_TILE_SIZE = (SCREEN_WIDTH / SPRITE_SIZE) * (SCREEN_HEIGHT / SPRITE_SIZE);

const uint8_t FADE_STEP = 3;

const uint8_t ENTITY_DEATH_PARTICLE_COUNT = 100;
const float ENTITY_DEATH_PARTICLE_GRAVITY_X = 0.0f;
const float ENTITY_DEATH_PARTICLE_GRAVITY_Y = 60.0f;
const float ENTITY_DEATH_PARTICLE_AGE = 1.0f;
const float ENTITY_DEATH_PARTICLE_SPEED = 60.0f;

const uint8_t CHECKPOINT_FRAMES = 4;
const float CHECKPOINT_FRAME_LENGTH = 0.2f;

const float PLAYER_SLOW_PARTICLE_AGE = 0.8f;

const uint8_t SNOW_WORLD = 1;

#ifdef PICO_BUILD
const uint8_t LEVEL_COUNT = 11;
#else
const uint8_t LEVEL_COUNT = 10;
#endif // PICO_BUILD

const uint8_t LEVEL_SELECT_NUMBER = LEVEL_COUNT + 2;
const uint8_t LEVELS_PER_WORLD = 4;

const uint16_t DEFAULT_VOLUME = 0x5000;

const uint16_t BYTE_SIZE = 256;

const float TEXT_FLASH_TIME = 0.8f;

void render_sprite(uint16_t id, blit::Point point);
void render_sprite(uint16_t id, blit::Point point, blit::SpriteTransform transform);

struct PlayerSaveData {
    uint8_t levelReached;
};

struct GameSaveData {
    uint16_t version;
    uint8_t inputType;
    // Settings values
    bool checkpoints = false;
    bool musicVolume = true;
    bool sfxVolume = true;
    uint8_t hackyFastMode = 0;
};

struct LevelData {
    uint16_t levelWidth, levelHeight;
};

struct LevelSaveData {
    uint8_t score;
    uint8_t enemiesKilled;
    float time;
};

// For version handling
struct GameVersion {
    uint8_t major;
    uint8_t minor;
    uint8_t build;
};

const uint8_t MESSAGE_STRINGS_COUNT = 7;
const uint8_t INPUT_TYPE_COUNT = 2;
const std::string messageStrings[MESSAGE_STRINGS_COUNT][INPUT_TYPE_COUNT] = {
        {
                "Press A to Start",
                "Press U to Start"
        },
        {
                "Press A to Skip",
                "Press U to Skip"
        },
        {
                "Press A",
                "Press U"
        },
        {
                "Press Y to Resume",
                "Press P to Resume"
        },
        {
                "Press A to Select",
                "Press U to Select"
        },
        {
                "Y - Back",
                "P - Back"
        },
        {
                "A - Toggle",
                "U - Toggle"
        }
};

//------------------------------------------------------------


const float CAMERA_PAN_TIME = 7.0f;
const float CAMERA_PAN_TIME_FINAL_LEVEL = 2.0f;
const float CAMERA_NEW_WORLD_TIME = 3.0f;
const float NEW_WORLD_DELAY_TIME = 2.0f;
const float CAMERA_RESPAWN_LOCK_MIN = 1.0f;


const float SNOW_LEVEL_GENERATE_DELAY = 0.1f;
const float SNOW_LEVEL_SELECT_GENERATE_DELAY = 0.15f;

const float REPEL_PLAYER_MIN = 5.0f;

const float FINISH_PARTICLE_SPAWN_DELAY = 0.05f;




const float LEVEL_INFO_MAX_RANGE = SPRITE_SIZE * 4;


const uint8_t NO_LEVEL_SELECTED = 255;

const float CONFETTI_GENERATE_DELAY = 0.05f;

const float THANKYOU_SPEED = 0.3f;


#ifdef PICO_BUILD
const uint16_t TEXT_BORDER = SPRITE_SIZE;

#else
const uint16_t TEXT_BORDER = SPRITE_SIZE * 2;
#endif // PICO_BUILD

// NOTE: all positions (x,y) mark TOP LEFT corner of sprites

// NOTE: issue with rendering (tiles on left are a pixel out sometimes) is due to integers being added to floats. Something along lines of (int)floorf(camera.pos.x) etc is recommended, but when I tried it I got strange results.
// If I implement that again, remember that all float calcs should be done, *then* casted, rather than casting each to int then adding etc


#ifdef PICO_BUILD
const uint8_t SETTINGS_COUNT = 3;

const std::string COINS_COLLECTED = "Coins:";
const std::string ENEMIES_KILLED = "Enemies:";
const std::string TIME_TAKEN = "Time:";
#else
const uint8_t SETTINGS_COUNT = 2;

const std::string COINS_COLLECTED = "Coins collected:";
const std::string ENEMIES_KILLED = "Enemies killed:";
const std::string TIME_TAKEN = "Time taken:";
#endif // PICO_BUILD

const uint8_t enemyHealths[] = { 1, 1, 1, 1, 2, 2, 2, 2, 1 };

const uint8_t levelTargets[LEVEL_COUNT][2][2] = {
        {
                // Level 1
                {
                        // Score
                        25, // Gold
                        20 // Silver
                },
                {
                        // Enemies killed
                        4, // Gold
                        3 // Silver
                }
        },
        {
                // Level 2
                {
                        // Score
                        22, // Gold
                        18 // Silver
                },
                {
                        // Enemies killed
                        5, // Gold
                        4 // Silver
                }
        },
        {
                // Level 3
                {
                        // Score
                        22, // Gold
                        18 // Silver
                },
                {
                        // Enemies killed
                        7, // Gold
                        5 // Silver
                }
        },
        {
                // Level 4
                {
                        // Score
                        10, // Gold
                        8 // Silver
                },
                {
                        // Enemies killed
                        14, // Gold
                        10 // Silver
                }
        },
        {
                // Level 5
                {
                        // Score
                        23, // Gold
                        19 // Silver
                },
                {
                        // Enemies killed
                        9, // Gold
                        7 // Silver
                }
        },
        {
                // Level 6
                {
                        // Score
                        25, // Gold
                        20 // Silver
                },
                {
                        // Enemies killed
                        10, // Gold
                        7 // Silver
                }
        },
        {
                // Level 7
                {
                        // Score
                        21, // Gold
                        17 // Silver
                },
                {
                        // Enemies killed
                        8, // Gold
                        6 // Silver
                }
        },
        {
                // Level 8
                {
                        // Score
                        20, // Gold
                        16 // Silver
                },
                {
                        // Enemies killed
                        10, // Gold
                        8 // Silver
                }
        },
#ifdef PICO_BUILD
        {
        // Level 9
        {
            // Score
            24, // Gold
            19 // Silver
        },
        {
            // Enemies killed
            10, // Gold
            8 // Silver
        }
    },
    {
        // Level 10
        {
            // Score
            22, // Gold
            18 // Silver
        },
        {
            // Enemies killed
            16, // Gold
            13 // Silver
        }
    },
#else
        {
                // Level 9
                {
                        // Score
                        32, // Gold
                        26 // Silver
                },
                {
                        // Enemies killed
                        21, // Gold
                        16 // Silver
                }
        },
#endif // PICO_BUILD
        {
                // End Level
                {
                        // Score
                        16, // Gold
                        13 // Silver
                },
                {
                        // Enemies killed
                        0, // Gold
                        0 // Silver
                }
        }
};

const float levelTargetTimes[LEVEL_COUNT][2] = {
        // Level 1
        {
                // Time
                12.5f, // Gold
                14.0f // Silver
        },
        // Level 2
        {
                // Time
                12.5f, // Gold
                14.5f // Silver
        },
        // Level 3
        {
                // Time
                12.5f, // Gold
                14.5f // Silver
        },
        // Level 4
        {
                // Time
                38.0f, // Gold
                47.0f // Silver
        },
        // Level 5
        {
                // Time
                13.0f, // Gold
                15.5f // Silver
        },
        // Level 6
        {
                // Time
                13.0f, // Gold
                16.0f // Silver
        },
        // Level 7
        {
                // Time
                13.0f, // Gold
                16.5f // Silver
        },
        // Level 8
        {
                // Time
                44.0f, // Gold
                55.0f // Silver
        },
#ifdef PICO_BUILD
        // Level 9
    {
        // Time
        20.0f, // Gold
        24.0f // Silver
    },
    // Level 10
    {
        // Time
        80.0f, // Gold TO CHECK + CHANGE IF NECESSARY
        100.0f // Silver TO CHECK + CHANGE IF NECESSARY
    },
#else
        // Level 9
        {
                // Time
                120.0f, // Gold
                180.0f // Silver
        },
#endif
        // End level
        {
                // Time
                4.2f, // Gold
                4.5f // Silver
        }
};

const std::vector<uint16_t> snowParticleImages = {
        464,
        465
};

const std::vector<uint16_t> confettiParticleImages = {
        484,
        485,
        486,
        487,
        488,
        489,
        490,
        491
};

#endif //SUPER_SQUARE_BROS_CONSTANTS_H
