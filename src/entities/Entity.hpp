#ifndef SUPER_SQUARE_BROS_ENTITY_HPP
#define SUPER_SQUARE_BROS_ENTITY_HPP

#include "../Camera.hpp"
#include "../Effects.hpp"
#include "../GameInput.hpp"
#include "../LevelObjects.hpp"
#include "../audio/Audio.hpp"
#include "../Projectile.hpp"

const float GRAVITY = 600.0f;
const float GRAVITY_MAX = 190.0f;

class Entity {
protected:
    uint16_t anchorFrame;
    bool deathParticles;
    float immuneTimer;

public:
    /// pos
    blit::Vec2 pos;
    /// vel
    blit::Vec2 vel;
    uint8_t health;
    bool locked;
    std::vector <Particle> particles;
    uint8_t lastDirection;
    float jumpCooldown;

    Entity();
    Entity(uint16_t xPosition, uint16_t yPosition, uint16_t frame, uint8_t startHealth);
    void update(float dt, ButtonStates& buttonStates);
    void update_collisions();
    void jump(float jumpVel, float cooldown);
    bool is_on_block();
    void render(Camera& camera);
    void set_immune();
    bool is_immune();

protected:
    void check_vertical_map_collisions();
    void check_horizontal_map_collisions();
};

class Enemy : public Entity {
public:
    float* playerX;
    float* playerY;

    Enemy();
    Enemy(uint16_t xPosition, uint16_t yPosition, uint8_t startHealth, uint8_t type);
    void update(float dt, ButtonStates& buttonStates);
    void render(Camera& camera);
    void set_player_position(float* x, float* y);
    void set_speed(float speed);
    uint8_t get_state();

protected:
    enum class EnemyType {
        BASIC, // type 1
        RANGED, // type 2
        PURSUIT, // type 3
        FLYING, // type 4
        ARMOURED, // type 5
        ARMOURED_RANGED, // type 6
        ARMOURED_PURSUIT, // type 7
        ARMOURED_FLYING, // type 8
        SHOOTING // type 9
    } enemyType;

    float reloadTimer;

    // Used for SHOOTING enemy
    float rapidfireTimer;
    uint8_t shotsLeft;

    float currentSpeed;

    uint8_t state;

    //enum EntityState {
    //    IDLE,
    //    WALK,
    //    //RUN,
    //    JUMP,
    //    //CROUCH,
    //    //INJURED,
    //    DEAD
    //} state;

private:
    bool shouldDirectionBeReversed();
    bool isAboutToBeOnBlock(float next_x_value);
    bool isWalkedIntoSideOfBlock(float next_x_value);
};

const uint8_t PLAYER_MAX_HEALTH = 3;

class Player : public Entity {
public:
    uint8_t score;
    uint8_t enemiesKilled;
    float levelTimer;
    uint8_t id;
    uint8_t lives;

    Player();
    Player(uint16_t xPosition, uint16_t yPosition, uint8_t colour);
    void update(float dt, ButtonStates& buttonStates);
    void update_collisions();
    void render(Camera& camera);

    bool colliding(Enemy& enemy);
    bool colliding(LevelTrigger& levelTrigger);
    bool colliding(Checkpoint& c);

protected:
    //enum EntityState {
    //    IDLE,
    //    WALK,
    //    //RUN,
    //    JUMP,
    //    //CROUCH,
    //    //INJURED,
    //    DEAD
    //} state;
    std::vector<BrownianParticle> slowParticles;
    float slowPlayerParticleTimer;
    float airTime;
};

#endif //SUPER_SQUARE_BROS_ENTITY_HPP
