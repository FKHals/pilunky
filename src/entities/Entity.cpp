#include "Entity.hpp"

#include "../GameState.hpp"


const float PLAYER_IMMUNE_TIME = 2.0f;

Entity::Entity() {
    pos = blit::Vec2(0, 0);
    vel = blit::Vec2(0, 0);

    anchorFrame = 0;

    lastDirection = 1; // 1 = right, 0 = left
    //state = IDLE;
    locked = false;
    deathParticles = false;

    health = 1;

    immuneTimer = 0;
    jumpCooldown = 0;
}

Entity::Entity(uint16_t xPosition, uint16_t yPosition, uint16_t frame, uint8_t startHealth): Entity() {
    pos.x = xPosition;
    pos.y = yPosition;
    vel.x = 0;
    vel.y = 0;

    anchorFrame = frame;

    health = startHealth;
}

void Entity::update(float dt, ButtonStates& buttonStates) {

}

/**
 * Where (in which corners) should the collisions be checked exactly:
 * It depends on whether a horizontal check or a vertical check is done!
 * # - pixel
 * @ - corner pixel that needs to be checked
 *
 *                         Top-left-(vertical-check)
 *                         |             Top-right-(vertical-check)
 *                         v             v
 * Top-left-               @             @
 * (horizontal-check) -> @ # # # # # # # # @ <- Top-right-
 *                         # # # # # # # #      (horizontal-check)
 *                         # S P R I T E #
 *                         # P I X E L S #
 *                         # # # # # # # #
 *                         # # # # # # # #
 * Bottom-left-            # # # # # # # #
 * (horizontal-check) -> @ # # # # # # # # @ <- Bottom-right-
 *                         @             @      (horizontal-check)
 *                         ^             ^
 *                         |             Bottom-right-(vertical-check)
 *                         Bottom-left-(vertical-check)
 */

/**
 * Check for vertical collisions with collidable map tiles.
 * (BEWARE: Positive velocity -> downwards, negative velocity -> upwards)
 */
void Entity::check_vertical_map_collisions() {
    const uint16_t x_pos_on_tile_map = (uint16_t)pos.x / SPRITE_SIZE;
    const uint16_t y_pos_on_tile_map = (uint16_t)pos.y / SPRITE_SIZE;
    // The (SPRITE_SIZE - 1) is needed since it references the outermost pixel _inside_ the entity sprite
    const uint16_t right_checked_x_pos = (uint16_t)(pos.x + (SPRITE_SIZE - 1)) / SPRITE_SIZE;
    if (vel.y > 0) {
        const uint16_t bottom_checked_y_pos = y_pos_on_tile_map + 1;
        if (
                // check bottom-left corner for collisions
                (gameState.getMapTile(x_pos_on_tile_map, bottom_checked_y_pos).isCollidable())
                // check bottom-right corner for collisions
                || (gameState.getMapTile(right_checked_x_pos, bottom_checked_y_pos).isCollidable())
                ) {
            // collided while going downwards
            pos.y = y_pos_on_tile_map * SPRITE_SIZE;
            vel.y = 0;
        }
    }
    else if (vel.y < 0) {
        const uint16_t top_checked_y_pos = (uint16_t)(pos.y - 1) / SPRITE_SIZE;
        if (
            // check top-left corner for collisions
                (gameState.getMapTile(x_pos_on_tile_map, top_checked_y_pos).isCollidable())
                // check top-right corner for collisions
                || (gameState.getMapTile(right_checked_x_pos, top_checked_y_pos).isCollidable())
                ) {
            // Collided while going upwards
            pos.y = (top_checked_y_pos + 1) * SPRITE_SIZE;
            vel.y = 0;
        }
    }
}

/**
 * Check for horizontal collisions with collidable map tiles.
 * (Positive velocity -> rightwards, negative velocity -> leftwards)
 */
void Entity::check_horizontal_map_collisions() {
    const uint16_t x_pos_on_tile_map = (uint16_t)pos.x / SPRITE_SIZE;
    const uint16_t y_pos_on_tile_map = (uint16_t)pos.y / SPRITE_SIZE;
    const uint16_t bottom_checked_y_pos = (uint16_t)(pos.y + (SPRITE_SIZE - 1)) / SPRITE_SIZE;
    if (vel.x > 0) {
        const uint16_t right_checked_x_pos = x_pos_on_tile_map + 1;
        if (
            // check top-right corner for collisions
                (gameState.getMapTile(right_checked_x_pos, y_pos_on_tile_map).isCollidable())
                // check bottom-right corner for collisions
                || (gameState.getMapTile(right_checked_x_pos, bottom_checked_y_pos).isCollidable())
            ) {
            // collided from right
            pos.x = x_pos_on_tile_map * SPRITE_SIZE;
            vel.x = 0;
        }
    }
    else if (vel.x < 0) {
        const uint16_t left_checked_x_pos = (uint16_t)(pos.x - 1) / SPRITE_SIZE;
        if (
            // check top-left corner for collisions
                (gameState.getMapTile(left_checked_x_pos, y_pos_on_tile_map).isCollidable())
                // check bottom-left corner for collisions
                || (gameState.getMapTile(left_checked_x_pos, bottom_checked_y_pos).isCollidable())
            ) {
            // Collided from left
            pos.x = (left_checked_x_pos + 1) * SPRITE_SIZE;
            vel.x = 0;
        }
    }
}

void Entity::update_collisions() {
    if (!locked) {
        // Update gravity
        vel.y += GRAVITY * gameState.dt;
        vel.y = std::min(vel.y, (float)GRAVITY_MAX);

        // Move entity pos.y
        pos.y += vel.y * gameState.dt;

        //TODO What if multiple tiles have been skipped due to large velocity?

        check_vertical_map_collisions();

        // check horizontal collisions...
        if (vel.x != 0.0f) {
            // Move entity pos.x
            pos.x += vel.x * gameState.dt;

            check_horizontal_map_collisions();

            // remember direction for sprite drawing
            if (vel.x > 0) {
                lastDirection = 1;
            }
            else if (vel.x < 0) {
                lastDirection = 0;
            }
        }
    }
}

void Entity::jump(float jumpVel, float cooldown) {
    // Jump
    vel.y = -jumpVel;
    jumpCooldown = cooldown;
}

bool Entity::is_on_block() {
    // If the entity is not moving vertically it must be on a block
    if (vel.y == 0) {
        // On top of block
        return true;
    }

    // Is entity on a locked LevelTrigger?
    for (LevelTrigger& levelTrigger : gameState.levelTriggers) {
        if (pos.y + SPRITE_SIZE == levelTrigger.pos.y && levelTrigger.pos.x + SPRITE_SIZE - 1 > pos.x && levelTrigger.pos.x + 1 < pos.x + SPRITE_SIZE) {
            // On top of block
            if (gameState.allPlayerSaveData[gameState.playerSelected].levelReached < levelTrigger.levelNumber) {
                // LevelTrigger is locked
                return true;
            }
        }
    }

    // Not on a block
    return false;
}

void Entity::render(Camera& camera) {
    if (health != 0) {
        bool visible = false;

        if (immuneTimer) {
            uint16_t immuneTimer_ms = (uint16_t)(immuneTimer * 1000);
            if (immuneTimer_ms % 150 < 75) {
                visible = true;
            }
        }
        else {
            visible = true;
        }

        if (visible) {
            uint16_t frame = anchorFrame;

            if (vel.y < -50) {
                frame = anchorFrame + 1;
            }
            else if (vel.y > 160) {
                frame = anchorFrame + 2;
            }

            /*if (immuneTimer) {
                frame = anchorFrame + 3;
            }*/

            if (lastDirection == 1) {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y), SpriteTransform::HORIZONTAL);
                render_sprite(frame, blit::Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y), blit::HORIZONTAL);
            }
            else {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y));
                render_sprite(frame, blit::Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y));
            }
        }
    }

    // Particles
    for (Particle& particle : particles) {
        particle.render(camera);
    }
}

void Entity::set_immune() {
    immuneTimer = PLAYER_IMMUNE_TIME;
}

bool Entity::is_immune() {
    return immuneTimer;
}
