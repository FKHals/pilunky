#include "Entity.hpp"

#include "../GameState.hpp"


const float ENTITY_IDLE_SPEED = 40.0f;
const float ENTITY_PURSUIT_SPEED = 55.0f;
const float ENTITY_JUMP_SPEED = 160.0f;
const float ENTITY_JUMP_COOLDOWN = 0.5f;

const float RANGED_MAX_RANGE = 64.0f;
const float RANGED_RELOAD_TIME = 2.0f;
const float RANGED_PROJECTILE_X_VEL_SCALE = 0.8f;
const float RANGED_PROJECTILE_Y_VEL_SCALE = 0.5f;

const float SHOOTING_MAX_RANGE_X = 64.0f;
const float SHOOTING_MAX_RANGE_Y = 32.0f;
const float SHOOTING_RELOAD_TIME = 2.5f;
const float SHOOTING_RAPID_RELOAD_TIME = 0.5f;
const uint8_t SHOOTING_ENEMY_CLIP_SIZE = 2;

const float BULLET_PROJECTILE_SPEED = 64.0f;

const float PURSUIT_MAX_RANGE = 48.0f;

Enemy::Enemy() : Entity() {
    enemyType = EnemyType::BASIC;
    reloadTimer = 0;
    rapidfireTimer = 0;

    playerX = nullptr;
    playerY = nullptr;

    currentSpeed = ENTITY_IDLE_SPEED;

    state = 0;

    shotsLeft = 0;
}

Enemy::Enemy(uint16_t xPosition, uint16_t yPosition,
             uint8_t startHealth, uint8_t type) : Entity(xPosition, yPosition,
                                                         TILE_ID_ENEMY_1 + type * 4, startHealth) {
    enemyType = (EnemyType)type;
    reloadTimer = 0;
    rapidfireTimer = 0;

    playerX = nullptr;
    playerY = nullptr;

    currentSpeed = ENTITY_IDLE_SPEED;

    state = 0;

    if (enemyType == EnemyType::SHOOTING) {
        shotsLeft = SHOOTING_ENEMY_CLIP_SIZE;
    } else {
        shotsLeft = 0;
    }
}

bool Enemy::isAboutToBeOnBlock(float next_x_value) {
    return gameState.getMapTile((uint16_t)next_x_value / SPRITE_SIZE, (uint16_t)pos.y / SPRITE_SIZE + 1).isCollidable();
}

bool Enemy::isWalkedIntoSideOfBlock(float next_x_value) {
    return gameState.getMapTile((uint16_t)next_x_value / SPRITE_SIZE, (uint16_t)pos.y / SPRITE_SIZE).isCollidable();
}

bool Enemy::shouldDirectionBeReversed() {
    bool reverseDirection = true;

    float tempX = lastDirection ?
            pos.x + SPRITE_SIZE :
            // subtract a small value if the entity "hangs" in a corner
            pos.x - ((vel.x == 0) ? 1.f : 0.f);

    // check if the entity will still be on a block if it does its next steps
    if (isAboutToBeOnBlock(tempX)) {
        // About to be on block
        reverseDirection = false;
    }
    // check if the entity will walk into a block if it does its next steps
    if (isWalkedIntoSideOfBlock(tempX)) {
        // Walked into side of block
        reverseDirection = true;
    }

    return reverseDirection;
}

void Enemy::update(float dt, ButtonStates& buttonStates) {
    if (health > 0) {
        if (reloadTimer) {
            reloadTimer -= dt;
            if (reloadTimer < 0) {
                reloadTimer = 0;
            }
        }

        if (rapidfireTimer) {
            rapidfireTimer -= dt;
            if (rapidfireTimer < 0) {
                rapidfireTimer = 0;
            }
        }

        if (jumpCooldown) {
            jumpCooldown -= dt;
            if (jumpCooldown < 0) {
                jumpCooldown = 0;
            }
        }

        if (enemyType == EnemyType::BASIC || enemyType == EnemyType::ARMOURED) {
            // Consider adding acceleration?
            if (lastDirection) {
                vel.x = currentSpeed;
            }
            else {
                vel.x = -currentSpeed;
            }

            Entity::update_collisions();

            bool reverseDirection = shouldDirectionBeReversed();

            if (reverseDirection) {
                lastDirection = 1 - lastDirection;
            }

            if (health == 1) {
                // EnemyType::ARMOURED has helmet on, and 2 hp
                enemyType = EnemyType::BASIC;
                anchorFrame = TILE_ID_ENEMY_1 + (int)enemyType * 4;
            }
        }
        else if (enemyType == EnemyType::RANGED || enemyType == EnemyType::ARMOURED_RANGED) {
            Entity::update_collisions();

            lastDirection = *playerX < pos.x ? 0 : 1;

            if (std::abs(pos.x - *playerX) < RANGED_MAX_RANGE && std::abs(pos.y - *playerY) < RANGED_MAX_RANGE) {
                state = 1;
            }
            else {
                state = 0;
            }

            if (state == 1) {
                if (!reloadTimer) {
                    // Fire!
                    // Maybe make these values constants?
                    gameState.projectiles.push_back(Projectile(pos.x, pos.y, RANGED_PROJECTILE_X_VEL_SCALE * (*playerX - pos.x), -std::abs(pos.x - *playerX) * RANGED_PROJECTILE_Y_VEL_SCALE + (*playerY - pos.y) * RANGED_PROJECTILE_Y_VEL_SCALE, gameState.currentWorldNumber == SNOW_WORLD || gameState.currentLevelNumber == 8 || gameState.currentLevelNumber == 9 ? TILE_ID_ENEMY_PROJECTILE_SNOWBALL : TILE_ID_ENEMY_PROJECTILE_ROCK));
                    reloadTimer = RANGED_RELOAD_TIME;

                    gameState.audioHandler.play(6);
                }
            }

            if (health == 1) {
                // EnemyType::ARMOURED_RANGED has helmet on, and 2 hp
                enemyType = EnemyType::RANGED;
                anchorFrame = TILE_ID_ENEMY_1 + (int)enemyType * 4;
            }
        }
        else if (enemyType == EnemyType::PURSUIT || enemyType == EnemyType::ARMOURED_PURSUIT) {
            // Consider adding acceleration?
            if (lastDirection) {
                vel.x = currentSpeed;
            }
            else {
                vel.x = -currentSpeed;
            }

            Entity::update_collisions();


            if (std::abs(pos.x - *playerX) < PURSUIT_MAX_RANGE && std::abs(pos.y - *playerY) < PURSUIT_MAX_RANGE) {
                state = 1;
            }
            else {
                state = 0;
            }

            if (state == 0) {
                // Just patrol... (Same as basic enemy)
                currentSpeed = ENTITY_IDLE_SPEED;

                bool reverseDirection = shouldDirectionBeReversed();

                if (reverseDirection) {
                    lastDirection = 1 - lastDirection;
                }
            }
            else if (state == 1) {
                // Pursue!
                currentSpeed = ENTITY_PURSUIT_SPEED;

                lastDirection = *playerX < pos.x ? 0 : 1;

                bool shouldJump = true;

                float tempX = lastDirection ? pos.x + SPRITE_SIZE : pos.x - SPRITE_SIZE;

                if (shouldJump) {
                    if (isAboutToBeOnBlock(tempX)) {
                        // About to be on block
                        shouldJump = false;
                    }
                }

                if (!shouldJump) {
                    if (isWalkedIntoSideOfBlock(tempX)) {
                        // Walked into side of block
                        shouldJump = true;
                    }
                }

                if (shouldJump && jumpCooldown == 0) {
                    if (is_on_block()) {
                        jump(ENTITY_JUMP_SPEED, ENTITY_JUMP_COOLDOWN);
                    }
                }
            }

            if (health == 1) {
                // EnemyType::ARMOURED_PURSUIT has helmet on, and 2 hp
                enemyType = EnemyType::PURSUIT;
                anchorFrame = TILE_ID_ENEMY_1 + (int)enemyType * 4;
            }
        }
        else if (enemyType == EnemyType::FLYING || enemyType == EnemyType::ARMOURED_FLYING) {
            // Should it be EnemyType::BOUNCING instead?
            // Consider adding acceleration?
            if (lastDirection) {
                vel.x = currentSpeed;
            }
            else {
                vel.x = -currentSpeed;
            }

            Entity::update_collisions();


            bool reverseDirection = false;
            float tempX = lastDirection ? pos.x + SPRITE_SIZE : pos.x - SPRITE_SIZE;
            if (isWalkedIntoSideOfBlock(tempX)) {
                // Walked into side of block
                reverseDirection = true;
            }

            if (jumpCooldown == 0 && is_on_block()) {
                jump(ENTITY_JUMP_SPEED, ENTITY_JUMP_COOLDOWN);
            }

            if (reverseDirection) {
                lastDirection = 1 - lastDirection;
            }

            if (health == 1) {
                // EnemyType::ARMOURED_FLYING has helmet on, and 2 hp
                enemyType = EnemyType::FLYING;
                anchorFrame = TILE_ID_ENEMY_1 + (int)enemyType * 4;
            }
        }
        else if (enemyType == EnemyType::SHOOTING) {
            Entity::update_collisions();

            lastDirection = *playerX < pos.x ? 0 : 1;

            if (std::abs(pos.x - *playerX) < SHOOTING_MAX_RANGE_X && std::abs(pos.y - *playerY) < SHOOTING_MAX_RANGE_Y) {
                state = 1;
            }
            else {
                state = 0;
            }

            if (state == 1) {
                if (!reloadTimer && !shotsLeft) {
                    shotsLeft = SHOOTING_ENEMY_CLIP_SIZE;
                }
                if (shotsLeft && !rapidfireTimer) {
                    // Fire!
                    // Maybe make these values constants?
                    float magnitude = std::sqrt(std::pow(*playerX - pos.x, 2) + std::pow(*playerY - pos.y, 2));
                    gameState.projectiles.push_back(Projectile(pos.x, pos.y, BULLET_PROJECTILE_SPEED * (*playerX - pos.x) / magnitude, BULLET_PROJECTILE_SPEED * (*playerY - pos.y) / magnitude, TILE_ID_ENEMY_PROJECTILE_BULLET, false, SPRITE_QUARTER));
                    shotsLeft--;
                    rapidfireTimer = SHOOTING_RAPID_RELOAD_TIME;

                    if (!shotsLeft) {
                        reloadTimer = SHOOTING_RELOAD_TIME;
                    }

                    gameState.audioHandler.play(6);
                }
            }
        }


        if (pos.y > gameState.levelDeathBoundary) {
            health = 0;
            vel.x = vel.y = 0;
        }
    }

    if (health == 0) {
        //state = DEAD;

        if (deathParticles) {
            if (particles.size() == 0) {
                // No particles left
                //deathParticles = false;
            }
            else {
                for (Particle& particle : particles) {
                    particle.update(dt);
                }

                // Remove any particles which are too old
                particles.erase(std::remove_if(particles.begin(), particles.end(), [](Particle& particle) { return (particle.age >= ENTITY_DEATH_PARTICLE_AGE); }), particles.end());
            }
        }
        else {
            // Generate particles
            particles = generate_particles(pos.x + SPRITE_HALF, pos.y + SPRITE_HALF, ENTITY_DEATH_PARTICLE_GRAVITY_X, ENTITY_DEATH_PARTICLE_GRAVITY_Y, enemyDeathParticleColours[enemyType == EnemyType::SHOOTING ? 4 : ((uint8_t)enemyType % 4)], ENTITY_DEATH_PARTICLE_SPEED, ENTITY_DEATH_PARTICLE_COUNT);
            deathParticles = true;
            // Play enemydeath sfx
            gameState.audioHandler.play(3);
        }
    }
}

void Enemy::render(Camera& camera) {
    Entity::render(camera);
}

void Enemy::set_player_position(float* x, float* y) {
    playerX = x;
    playerY = y;
}

void Enemy::set_speed(float speed) {
    currentSpeed = speed;
}

uint8_t Enemy::get_state() {
    return state;
}