#include "Entity.hpp"

#include "../scenes/Scene.h"
#include "../GameState.hpp"


using namespace blit;


const uint8_t PLAYER_START_LIVES = 3;

const float PLAYER_MAX_JUMP = 190.0f;
//const float  PLAYER_ATTACK_JUMP = 90.0f;
const float PLAYER_ATTACK_JUMP_SCALE = 0.65f;
const float PLAYER_ATTACK_JUMP_MIN = 90.0f;
const float PLAYER_MAX_SPEED = 87.0f;
const float PLAYER_ACCELERATION = 370.0f;
const float PLAYER_DECELERATION = 330.0f;
const float PLAYER_JUMP_COOLDOWN = 0.4f;
const float PLAYER_JUMP_MIN_AIR_TIME = 0.05f;
//const float PLAYER_JUMP_EXTRA_GRAVITY = 500.0f;
const float PLAYER_JUMP_EXTRA_DOWNSCALE = 0.92f;

const float PLAYER_SLOW_MAX_JUMP = 110.0f;
const float PLAYER_SLOW_MAX_SPEED = 15.0f;
const float PLAYER_SLOW_ACCELERATION = 150.0f;
const float PLAYER_SLOW_DECELERATION = 100.0f;
const float PLAYER_SLOW_JUMP_COOLDOWN = 0.3f;

const float PLAYER_SLOW_PARTICLE_GRAVITY_X = 0.0f;
const float PLAYER_SLOW_PARTICLE_GRAVITY_Y = -30.0f;
const float PLAYER_SLOW_PARTICLE_SPEED = 50.0f;
const uint8_t PLAYER_SLOW_PARTICLE_WIGGLE = 1;
const float PLAYER_SLOW_PARTICLE_SPAWN_DELAY = 0.05f;


Player::Player() : Entity() {
    score = 0;
    enemiesKilled = 0;
    levelTimer = 0.0f;

    id = 0;

    lives = PLAYER_START_LIVES;

    slowPlayerParticleTimer = 0.0f;
    airTime = 0.0f;
}

Player::Player(uint16_t xPosition, uint16_t yPosition, uint8_t colour) : Entity(xPosition, yPosition, TILE_ID_PLAYER_1 + colour * 4, PLAYER_MAX_HEALTH) {
    score = 0;
    enemiesKilled = 0;
    levelTimer = 0.0f;

    id = colour;

    lives = PLAYER_START_LIVES;

    slowPlayerParticleTimer = 0.0f;
    airTime = 0.0f;
}

void Player::update(float dt, ButtonStates& buttonStates) {
    if (immuneTimer) {
        immuneTimer -= dt;
        if (immuneTimer < 0) {
            immuneTimer = 0;
        }
    }

    if (jumpCooldown) {
        jumpCooldown -= dt;
        if (jumpCooldown < 0) {
            jumpCooldown = 0;
        }
    }

    if (health > 0) {

#ifdef TESTING_MODE
        health = 3;

        if (buttonStates.X) {
            pos.x = finish.pos.x - SPRITE_SIZE * 2;
            pos.y = finish.pos.y;
        }
#endif // TESTING_MODE

        if (!locked) {
            levelTimer += dt;

            if (is_on_block()) {
                airTime = 0.0f;
            }
            else {
                airTime += dt;
            }

            if (buttonStates.A && jumpCooldown == 0 && airTime < PLAYER_JUMP_MIN_AIR_TIME) {
                jump(gameState.slowPlayer ? PLAYER_SLOW_MAX_JUMP : PLAYER_MAX_JUMP, gameState.slowPlayer ? PLAYER_SLOW_JUMP_COOLDOWN : PLAYER_JUMP_COOLDOWN);
                if (!gameState.currentScene->instanceOf(&scene::characterSelect)) {
                    // Play jump sfx
                    gameState.audioHandler.play(1);
                }
            }
            else if (!buttonStates.A && vel.y < 0) {
                vel.y *= PLAYER_JUMP_EXTRA_DOWNSCALE;
            }

            if (buttonStates.LEFT) {
                if (gameState.slowPlayer) {
                    vel.x -= PLAYER_SLOW_ACCELERATION * dt;
                    if (vel.x < -PLAYER_SLOW_MAX_SPEED) {
                        vel.x = -PLAYER_SLOW_MAX_SPEED;
                    }
                }
                else {
                    vel.x -= PLAYER_ACCELERATION * dt;
                    if (vel.x < -PLAYER_MAX_SPEED) {
                        vel.x = -PLAYER_MAX_SPEED;
                    }
                }
            }
            else if (buttonStates.RIGHT) {
                if (gameState.slowPlayer) {
                    vel.x += PLAYER_SLOW_ACCELERATION * dt;
                    if (vel.x > PLAYER_SLOW_MAX_SPEED) {
                        vel.x = PLAYER_SLOW_MAX_SPEED;
                    }
                }
                else {
                    vel.x += PLAYER_ACCELERATION * dt;
                    if (vel.x > PLAYER_MAX_SPEED) {
                        vel.x = PLAYER_MAX_SPEED;
                    }
                }
            }
            else {
                if (vel.x > 0) {
                    if (gameState.slowPlayer) {
                        vel.x -= PLAYER_SLOW_DECELERATION * dt;
                    }
                    else {
                        vel.x -= PLAYER_DECELERATION * dt;
                    }

                    if (vel.x < 0) {
                        vel.x = 0;
                    }
                }
                else if (vel.x < 0) {
                    if (gameState.slowPlayer) {
                        vel.x += PLAYER_SLOW_DECELERATION * dt;
                    }
                    else {
                        vel.x += PLAYER_DECELERATION * dt;
                    }

                    if (vel.x > 0) {
                        vel.x = 0;
                    }
                }
            }
        }

        uint8_t coinCount = gameState.coins.size();

        // Remove coins if player jumps on them
        gameState.coins.erase(std::remove_if(gameState.coins.begin(), gameState.coins.end(),
                                               [this](Coin& coin) { return (coin.pos.x + SPRITE_SIZE > pos.x && coin.pos.x < pos.x + SPRITE_SIZE && coin.pos.y + SPRITE_SIZE > pos.y && coin.pos.y < pos.y + SPRITE_SIZE); }),
                                gameState.coins.end());

        // Add points to player score (1 point per coin which has been deleted)
        score += coinCount - gameState.coins.size();

        if (coinCount != gameState.coins.size()) {
            // Must have picked up a coin
            // Play coin sfx
            gameState.audioHandler.play(gameState.coinSfxAlternator ? 0 : 2);
            gameState.coinSfxAlternator = !gameState.coinSfxAlternator;
        }


        uint8_t enemyCount = gameState.enemies.size();// + bosses.size();

        // Remove enemies if no health left
        gameState.enemies.erase(std::remove_if(gameState.enemies.begin(), gameState.enemies.end(),
                                                 [](Enemy& enemy) { return (enemy.health == 0 && enemy.particles.size() == 0); }),
                                  gameState.enemies.end());

        enemiesKilled += enemyCount - gameState.enemies.size();// - bosses.size();

        update_collisions();

        /*
        if (!is_immune()) {
            for (Tile& spike : gameState.spikes) {
                if (Entity::colliding(spike)) {
                    if ((spike.get_id() == TILE_ID_SPIKE_BOTTOM && pos.y + SPRITE_SIZE >= spike.pos.y + SPRITE_HALF) ||
                        (spike.get_id() == TILE_ID_SPIKE_TOP && pos.y <= spike.pos.y + SPRITE_HALF) ||
                        (spike.get_id() == TILE_ID_SPIKE_LEFT && pos.x <= spike.pos.x + SPRITE_HALF) ||
                        (spike.get_id() == TILE_ID_SPIKE_RIGHT && pos.x + SPRITE_SIZE >= spike.pos.x + SPRITE_HALF)) {

                        health -= 1;
                        set_immune();
                        break;
                    }
                }
            }
        }
         */

        if (pos.y > gameState.levelDeathBoundary) {
            health = 0;
            vel.x = vel.y = 0;
        }
    }

    if (gameState.slowPlayer || gameState.repelPlayer) {
        slowPlayerParticleTimer += dt;
        if (slowPlayerParticleTimer >= PLAYER_SLOW_PARTICLE_SPAWN_DELAY) {
            slowPlayerParticleTimer -= PLAYER_SLOW_PARTICLE_SPAWN_DELAY;
            slowParticles.push_back(generate_brownian_particle(pos.x + SPRITE_HALF, pos.y + SPRITE_HALF, PLAYER_SLOW_PARTICLE_GRAVITY_X, PLAYER_SLOW_PARTICLE_GRAVITY_Y, PLAYER_SLOW_PARTICLE_SPEED, gameState.repelPlayer ? repelPlayerParticleColours : slowPlayerParticleColours, PLAYER_SLOW_PARTICLE_WIGGLE));
        }
    }
    else {
        slowPlayerParticleTimer = 0.0f;
    }

    for (BrownianParticle& slowParticle : slowParticles) {
        slowParticle.update(dt);
    }
    // Remove any particles which are too old
    slowParticles.erase(std::remove_if(slowParticles.begin(), slowParticles.end(), [](BrownianParticle& particle) { return (particle.age >= PLAYER_SLOW_PARTICLE_AGE); }), slowParticles.end());



    if (health == 0) {
        //state = DEAD;

        if (deathParticles) {
            if (particles.size() == 0) {
                // No particles left, reset values which need to be

                deathParticles = false;

                // If player has lives left, respawn
                if (lives) {
                    // Reset player pos and health, maybe remove all this?
                    health = 3;
                    vel.x = vel.y = 0;
                    lastDirection = 1;

                    // Go to checkpoint if checkpoints are enabled, and have reached it.
                    if (gameState.gameSaveData.checkpoints && gameState.checkpoint.colour) {
                        pos.x = gameState.checkpoint.pos.x;
                        pos.y = gameState.checkpoint.pos.y;
                    }
                    else {
                        pos.x = gameState.playerStartX;
                        pos.y = gameState.playerStartY;
                    }

                    // Stop player from moving while respawning
                    gameState.cameraRespawn = true;
                    locked = true;
                    gameState.slowPlayer = false;
                    gameState.dropPlayer = false;
                    gameState.repelPlayer = false;
                    gameState.bossBattle = false;

                    // Make player immune when respawning?
                    //set_immune();

                    // Remove immunity when respawning
                    immuneTimer = 0;
                }
            }
            else {
                for (Particle& particle : particles) {
                    particle.update(dt);
                }

                // Remove any particles which are too old
                particles.erase(std::remove_if(particles.begin(), particles.end(), [](Particle& particle) { return (particle.age >= ENTITY_DEATH_PARTICLE_AGE); }), particles.end());
            }
        }
        else if (lives) {
            // Generate particles
            particles = generate_particles(pos.x + SPRITE_HALF, pos.y + SPRITE_HALF, ENTITY_DEATH_PARTICLE_GRAVITY_X, ENTITY_DEATH_PARTICLE_GRAVITY_Y, playerDeathParticleColours[id], ENTITY_DEATH_PARTICLE_SPEED, ENTITY_DEATH_PARTICLE_COUNT);
            deathParticles = true;

            // Reduce player lives by one
            lives--;

            gameState.audioHandler.play(5);
        }
    }
}

//TODO Deduplicate with Entity::update_collisions()
void Player::update_collisions() {
    if (!locked) {
        // Update gravity
        vel.y += GRAVITY * gameState.dt;
        vel.y = std::min(vel.y, GRAVITY_MAX);

        // Move entity pos.y
        pos.y += vel.y * gameState.dt;

        // Here check collisions...

        // Enemies first
        for (Enemy& enemy : gameState.enemies) {
            if (enemy.health && colliding(enemy)) {
                if (pos.y + SPRITE_SIZE < enemy.pos.y + SPRITE_QUARTER) {
                    // Collided from top
                    pos.y = enemy.pos.y - SPRITE_SIZE;

                    if (vel.y > 0.0f || enemy.vel.y < 0) { // && !enemies[i].is_immune()
                        //vel.y = -PLAYER_ATTACK_JUMP;
                        vel.y = -std::max(vel.y * PLAYER_ATTACK_JUMP_SCALE, PLAYER_ATTACK_JUMP_MIN);

                        // Take health off enemy
                        enemy.health--;

                        // Play enemy injured sfx
                        if (enemy.health) {
                            gameState.audioHandler.play(4);
                        }

                        if (enemy.vel.y < 0) {
                            // Enemy is jumping
                            // Stop enemy's jump
                            enemy.vel.y = 0;
                        }
                    }
                }
            }
        }

        check_vertical_map_collisions();

        if (vel.y != 0.0f) {
            for (LevelTrigger& levelTrigger : gameState.levelTriggers) {
                if (levelTrigger.visible && colliding(levelTrigger)) {
                    if (vel.y > 0 && pos.y + SPRITE_SIZE < levelTrigger.pos.y + SPRITE_HALF) {
                        if (gameState.allPlayerSaveData[gameState.playerSelected].levelReached >= levelTrigger.levelNumber) {
                            // Level is unlocked

                            // Collided from top
                            pos.y = levelTrigger.pos.y - SPRITE_SIZE;
                            //vel.y = -PLAYER_ATTACK_JUMP;
                            vel.y = -std::max(vel.y * PLAYER_ATTACK_JUMP_SCALE, PLAYER_ATTACK_JUMP_MIN);

                            levelTrigger.set_active();

                            // Play sfx
                            gameState.audioHandler.play(4);
                        }
                        else {
                            // Level is locked, act as a solid object

                            // Collided from top
                            pos.y = levelTrigger.pos.y - SPRITE_SIZE;
                            vel.y = 0;
                        }
                    }
                }
            }
        }

        // Move entity pos.x
        if (vel.x != 0.0f) {
            pos.x += vel.x * gameState.dt;

            check_horizontal_map_collisions();

            for (LevelTrigger& levelTrigger : gameState.levelTriggers) {
                if (levelTrigger.visible && colliding(levelTrigger)) {
                    if (vel.x > 0) {
                        // Collided from left
                        pos.x = levelTrigger.pos.x - SPRITE_SIZE;
                    }
                    else if (vel.x < 0) {
                        // Collided from right
                        pos.x = levelTrigger.pos.x + SPRITE_SIZE;
                    }
                    vel.x = 0;
                    break;
                }
            }

            if (vel.x > 0) {
                lastDirection = 1;
            }
            else if (vel.x < 0) {
                lastDirection = 0;
            }
        }

        if (!immuneTimer && !gameState.dropPlayer) {
            for (Enemy& enemy : gameState.enemies) {
                if (colliding(enemy) && enemy.health) {
                    health--;
                    set_immune();
                }
            }
        }

        if (colliding(gameState.checkpoint)) {
            if (gameState.checkpoint.activate(id + 1)) {
                gameState.audioHandler.play(4); // need to load + play activate sound - swap with playerdeath
            }
        }

        if (pos.x < -SCREEN_MID_WIDTH) {
            pos.x = -SCREEN_MID_WIDTH;
        }
        else if (pos.x > gameState.levelData.levelWidth * SPRITE_SIZE + SCREEN_MID_WIDTH) {
            pos.x = gameState.levelData.levelWidth * SPRITE_SIZE + SCREEN_MID_WIDTH;
        }
    }
}

void Player::render(Camera& camera) {
    // Particles
    for (BrownianParticle& slowParticle : slowParticles) {
        slowParticle.render(camera);
    }

    if (health != 0) {
        bool visible = false;

        if (immuneTimer) {
            uint16_t immuneTimer_ms = (uint16_t)(immuneTimer * 1000);
            if (immuneTimer_ms % 150 < 75) {
                visible = true;
            }
        }
        else {
            visible = true;
        }

        if (visible) {
            uint16_t frame = anchorFrame;

            if (vel.y < -50) {
                frame = anchorFrame + 1;
            }
            else if (vel.y > 160) {
                frame = anchorFrame + 2;
            }

            if (lastDirection == 1) {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y), SpriteTransform::HORIZONTAL);
                render_sprite(frame, Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y), SpriteTransform::HORIZONTAL);
            }
            else {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y));
                render_sprite(frame, Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y));
            }


            if (gameState.slowPlayer || gameState.repelPlayer) {
                render_sprite(448, Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y));
            }
        }
    }

    // Particles
    for (Particle& particle : particles) {
        particle.render(camera);
    }
}

//using Entity::colliding;

bool Player::colliding(Enemy& enemy) {
    // Replace use of this with actual code?
    return (enemy.pos.x + SPRITE_SIZE > pos.x && enemy.pos.x < pos.x + SPRITE_SIZE && enemy.pos.y + SPRITE_SIZE > pos.y && enemy.pos.y < pos.y + SPRITE_SIZE);
}

bool Player::colliding(LevelTrigger& levelTrigger) {
    // Replace use of this with actual code?
    return (levelTrigger.pos.x + SPRITE_SIZE > pos.x && levelTrigger.pos.x < pos.x + SPRITE_SIZE && levelTrigger.pos.y + SPRITE_SIZE > pos.y && levelTrigger.pos.y < pos.y + SPRITE_SIZE);
}

bool Player::colliding(Checkpoint& c) {
    return (c.pos.x + SPRITE_SIZE > pos.x && c.pos.x < pos.x + SPRITE_SIZE && c.pos.y + SPRITE_SIZE > pos.y && c.pos.y - SPRITE_SIZE < pos.y + SPRITE_SIZE);
}
