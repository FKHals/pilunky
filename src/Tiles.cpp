#include "Tiles.hpp"

#include "LevelObjects.hpp"

// TODO Put this somewhere else?
const uint16_t TILE_ID_SOLID = 4;

const Tile Tile::EMPTY = Tile{0, Tile::NONE, TILE_ID_EMPTY};
const Tile Tile::SOLID = Tile{1, Tile::COLLIDABLE, TILE_ID_SOLID};