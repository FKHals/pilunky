#ifndef SUPER_SQUARE_BROS_DATA_H
#define SUPER_SQUARE_BROS_DATA_H


#include "Constants.hpp"

uint16_t get_version(GameVersion gameVersionData);
GameVersion get_version_struct(uint16_t version);
void save_game_data();
void save_level_data(uint8_t playerID, uint8_t levelNumber);
void save_player_data(uint8_t playerID);
LevelSaveData load_level_data(uint8_t playerID, uint8_t levelNumber);
PlayerSaveData load_player_data(uint8_t playerID);
void load_level(uint8_t levelNumber);

#endif //SUPER_SQUARE_BROS_DATA_H
