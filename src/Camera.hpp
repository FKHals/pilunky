#ifndef SUPER_SQUARE_BROS_CAMERA_HPP
#define SUPER_SQUARE_BROS_CAMERA_HPP

#include <types/vec2.hpp>

const blit::Vec2 CAMERA_SCALE = blit::Vec2(10.0f, 5.0f);

class Camera {
public:
    blit::Vec2 pos;
    blit::Vec2 temp;

    bool locked;

    Camera();
    void reset_temp();
    void reset_timer();
    bool timer_started();
    void start_timer();
    void update_timer(float dt);
    float get_timer();
    void ease_out_to(float dt, blit::Vec2 target);
    void linear_to(float dt, float startX, float startY, float targetX, float targetY, float time);
protected:
    float timer;
};


const float SCREEN_SHAKE_SHAKINESS = 3.0f; //pixels either way - maybe more?

class ScreenShake {
public:
    ScreenShake();
    ScreenShake(float shakiness);
    void set_shake(float shake);
    void set_shakiness(float shakiness);
    float time_to_shake(float dt);

protected:
    float shake;
    float shakiness;
};

#endif //SUPER_SQUARE_BROS_CAMERA_HPP
