#include "Effects.hpp"

#include "engine/engine.hpp"
#include "Constants.hpp"


using namespace blit;


Colour::Colour() {
    r = g = b = a = 255;
}

Colour::Colour(uint8_t r, uint8_t g, uint8_t b) {
    this->r = r;
    this->g = g;
    this->b = b;
    a = 255;
}

Colour::Colour(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
}


Particle::Particle() {
    pos = blit::Vec2(0, 0);
    vel = blit::Vec2(0, 0);
    gravity = blit::Vec2(0, 0);
    colour = Colour(0, 0, 0);

    age = 0;
}

Particle::Particle(float xPosition, float yPosition,
                   float xVelocity, float yVelocity,
                   float particleGravityX, float particleGravityY,
                   Colour particleColour) {
    pos.x = xPosition;
    pos.y = yPosition;
    vel.x = xVelocity;
    vel.y = yVelocity;

    gravity.x = particleGravityX;
    gravity.y = particleGravityY;
    colour = particleColour;

    age = 0;
}

void Particle::render(Camera& camera) {
    screen.pen = Pen(colour.r, colour.g, colour.b, colour.a);
    screen.pixel(Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y));
}

void Particle::update(float dt) {
    age += dt;
    colour.a = std::max(0.0f, colour.a - age * 10);

    vel += gravity * dt;
    pos += vel * dt;
}


ImageParticle::ImageParticle() {
    pos = blit::Vec2(0, 0);
    vel = blit::Vec2(0, 0);
    gravity = blit::Vec2(0, 0);
    id = 0;
}

ImageParticle::ImageParticle(float xPosition, float yPosition,
                             float xVelocity, float yVelocity,
                             float particleGravityX, float particleGravityY,
                             uint16_t tileID) {
    pos.x = xPosition;
    pos.y = yPosition;
    vel.x = xVelocity;
    vel.y = yVelocity;

    gravity.x = particleGravityX;
    gravity.y = particleGravityY;
    id = tileID;
}

void ImageParticle::render(Camera& camera) {
    render_sprite(id, Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y));
}

void ImageParticle::update(float dt) {
    vel += gravity * dt;
    pos += vel * dt;
}


BrownianParticle::BrownianParticle() : Particle() {
    angleWiggle = 1;
    angle = 0;
    speed = 0;
}

BrownianParticle::BrownianParticle(float xPosition, float yPosition, uint16_t angle, float speed, float particleGravityX, float particleGravityY, Colour particleColour, uint8_t angleWiggle) : Particle(xPosition, yPosition, 0, 0, particleGravityX, particleGravityY, particleColour) {
    this->angleWiggle = angleWiggle;
    this->angle = angle;
    this->speed = speed;
}

void BrownianParticle::update(float dt) {
    angle += (rand() % (angleWiggle * 2 + 1)) - angleWiggle;
    angle %= 360;

    vel.x = std::cos((float)angle) * speed;
    vel.y = std::sin((float)angle) * speed;

    Particle::update(dt);
}


std::vector<Particle> generate_particles(float x, float y,
                                         float gravityX, float gravityY,
                                         std::array<Colour, 3> colours, float speed, uint8_t count) {
    std::vector<Particle> particles;

    for (uint8_t i = 0; i < count; i++) {
        float angle = rand() % 360;

        float xVel = ((rand() % 100) / 100.0f) * std::cos(angle) * speed;
        float yVel = ((rand() % 100) / 100.0f) * std::sin(angle) * speed;

        particles.push_back(Particle(x, y, xVel, yVel, gravityX, gravityY, colours[rand() % colours.size()]));
    }

    return particles;
}

BrownianParticle generate_brownian_particle(float x, float y, float gravityX, float gravityY, float speed, std::array<Colour, 3> colours, uint8_t wiggle) {
    uint16_t angle = rand() % 360;

    return BrownianParticle(x, y, angle, speed, gravityX, gravityY, colours[rand() % colours.size()], wiggle);
}

AnimatedTransition::AnimatedTransition() {
    animationTimer = 0;
    currentFrame = 0;

    state = TransitionState::OPEN;
    x = y = 0;
    closedTimer = 0;
}

AnimatedTransition::AnimatedTransition(uint16_t xPosition, uint16_t yPosition, std::vector<uint16_t> open, std::vector<uint16_t> close) {
    animationTimer = 0;
    currentFrame = 0;

    openingFrames = open;
    closingFrames = close;
    state = TransitionState::OPEN;
    x = xPosition;
    y = yPosition;

    closedTimer = 0;
}

const float TRANSITION_FRAME_LENGTH = 0.1f;
const float TRANSITION_CLOSE_LENGTH = 0.5f;

void AnimatedTransition::update(float dt, ButtonStates& buttonStates) {
    if (state == TransitionState::CLOSING || state == TransitionState::OPENING) {
        animationTimer += dt;

        if (animationTimer >= TRANSITION_FRAME_LENGTH) {
            animationTimer -= TRANSITION_FRAME_LENGTH;
            currentFrame++;

            if (state == TransitionState::CLOSING) {
                if (currentFrame == closingFrames.size()) {
                    state = TransitionState::CLOSED;
                    closedTimer = 0;
                }
            }
            else {
                if (currentFrame == openingFrames.size()) {
                    state = TransitionState::OPEN;
                }
            }
        }
    }
    else if (state == TransitionState::CLOSED) {
        closedTimer += dt;
        if (closedTimer >= TRANSITION_CLOSE_LENGTH) {
            state = TransitionState::READY_TO_OPEN;
        }
    }
}

void AnimatedTransition::render(Camera& camera) {
    if (state == TransitionState::CLOSING) {
        //screen.sprite(closingFrames[currentFrame], Point(x, y));
        render_sprite(closingFrames[currentFrame], Point(x, y));
    }
    else if (state == TransitionState::OPENING) {
        //screen.sprite(openingFrames[currentFrame], Point(x, y), SpriteTransform::HORIZONTAL);
        render_sprite(openingFrames[currentFrame], Point(x, y), SpriteTransform::HORIZONTAL);
    }
    else if (state == TransitionState::CLOSED || state == TransitionState::READY_TO_OPEN) {
        //screen.sprite(closingFrames[closingFrames.size() - 1], Point(x, y));
        render_sprite(closingFrames[closingFrames.size() - 1], Point(x, y));
    }
    else if (state == TransitionState::OPEN) {
        // Don't do anything
    }
}

void AnimatedTransition::close() {
    state = TransitionState::CLOSING;
    animationTimer = 0;
    currentFrame = 0;
}

void AnimatedTransition::open() {
    state = TransitionState::OPENING;
    animationTimer = 0;
    currentFrame = 0;
}

bool AnimatedTransition::is_closed() {
    return state == TransitionState::CLOSED;
}

bool AnimatedTransition::is_ready_to_open() {
    return state == TransitionState::READY_TO_OPEN;
}

bool AnimatedTransition::is_open() {
    return state == TransitionState::OPEN;
}