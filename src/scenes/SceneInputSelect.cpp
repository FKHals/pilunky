#include "Scene.h"

#include "engine/engine.hpp"
#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


using namespace blit;

#ifdef PICO_BUILD
const std::string CONTROLLER_TEXT = "Pico";
#else
#ifdef TARGET_32BLIT_HW
const std::string CONTROLLER_TEXT = "32Blit";
#else
const std::string CONTROLLER_TEXT = "Controller";
#endif // TARGET_32BLIT_HW
#endif // PICO_BUILD

extern Colour splashColour;
extern ButtonStates buttonStates;
extern GameSaveData gameSaveData;
extern AudioHandler::AudioHandler audioHandler;

extern float textFlashTimer;
extern AnimatedTransition transition[];

namespace scene {

    void InputSelect::onSwitch() const {
        // do nothing
    }

    void InputSelect::render() const {
        render_background();

        background_rect(0);
        background_rect(1);


        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        screen.text("Select Input Method", minimal_font, Point(SCREEN_MID_WIDTH, 10), true, center_center);

        screen.pen = gameState.gameSaveData.inputType == CONTROLLER ? Pen(inputSelectColour.r, inputSelectColour.g,
                                                                            inputSelectColour.b) : Pen(defaultWhite.r,
                                                                                           defaultWhite.g,
                                                                                           defaultWhite.b);
        screen.text(CONTROLLER_TEXT, minimal_font, Point(SCREEN_MID_WIDTH, 50), true, center_center);

        screen.pen =
                gameState.gameSaveData.inputType == KEYBOARD ? Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b)
                                                               : Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        screen.text("Keyboard", minimal_font, Point(SCREEN_MID_WIDTH, 70), true, center_center);


        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);

        if (gameState.textFlashTimer < TEXT_FLASH_TIME * 0.6f) {
            screen.text(messageStrings[0][gameState.gameSaveData.inputType], minimal_font,
                        Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, center_center);
        }
    }

    void InputSelect::update() const {
        if (gameState.splashColour.a > 0) {
            if (gameState.splashColour.a >= FADE_STEP) {
                gameState.splashColour.a -= FADE_STEP;
            } else {
                gameState.splashColour.a = 0;
            }
        } else {
            if (gameState.transition[0].is_ready_to_open()) {
                switchSceneTo(&scene::menu);
            } else if (gameState.transition[0].is_open()) {
                if (gameState.gameSaveData.inputType == CONTROLLER) {
                    if (gameState.buttonStates.DOWN) {
                        gameState.gameSaveData.inputType = KEYBOARD;

                        gameState.audioHandler.play(0);
                    }
                } else if (gameState.gameSaveData.inputType == KEYBOARD) {
                    if (gameState.buttonStates.UP) {
                        gameState.gameSaveData.inputType = CONTROLLER;

                        gameState.audioHandler.play(0);
                    }
                }

                if (gameState.buttonStates.A == 2) {
                    gameState.audioHandler.play(0);

                    close_transition();

                    // Save inputType
                    save_game_data();
                }
            }
        }
    }
}