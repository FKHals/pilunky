#include "Scene.h"

#include "engine/engine.hpp"
#include "graphics/color.hpp"

#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


using namespace blit;

namespace scene {

    void InGame::onSwitch() const {
        // Load coin sfx slot into select sfx slot
        gameState.audioHandler.load(0, 2);

        // Load level
        load_level(gameState.currentLevelNumber);

        // Make sure player attribute setting is done after load_level call, since player is reassigned in that method
        gameState.player.locked = true;
        gameState.cameraIntro = true;
        reset_level_vars();

        open_transition();
    }

    void InGame::render() const {
        render_background();

        render_level();

        render_finish();

        if (gameState.currentLevelNumber == LEVEL_COUNT - 1) {
            // render thankyou
            {
                screen.pen = hsv_to_rgba(gameState.thankyouValue, 1.0f, 1.0f);

                screen.text("Thanks for playing!", minimal_font,
                            Rect(gameState.checkpoint.pos.x - SPRITE_SIZE * 6 - gameState.camera.pos.x + SCREEN_MID_WIDTH,
                                 gameState.checkpoint.pos.y - SPRITE_SIZE * 7 - gameState.camera.pos.y + SCREEN_MID_HEIGHT, SPRITE_SIZE * 12,
                                 SPRITE_SIZE * 2), true, center_center);
            }
        }

        render_entities();

        render_particles();


        if (gameState.gamePaused) {
            screen.pen = Pen(gameBackground.r, gameBackground.g, gameBackground.b,
                             hudBackground.a); // use hudBackground.a to make background semi transparent
            screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
            screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
            screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)); // repeat to make darker

            //screen.pen = Pen(hudBackground.r, hudBackground.g, hudBackground.b, hudBackground.a);
            //screen.rectangle(Rect(0, SPRITE_SIZE + 12, SCREEN_WIDTH, SCREEN_HEIGHT - (SPRITE_SIZE + 12))); // do it twice to make it darker

            background_rect(0);
            background_rect(1);


            //screen.pen = Pen(hudBackground.r, hudBackground.g, hudBackground.b, hudBackground.a);
            //screen.text(messageStrings[3][gameSaveData.inputType], minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, TextAlign::center_center);

            screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
            display_stats(false);
            screen.text("Game Paused", minimal_font, Point(SCREEN_MID_WIDTH, 10), true, center_center);

            if (gameState.pauseMenuItem == 0) {
                screen.pen = Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b);
            }
            screen.text("Resume", minimal_font, Point(TEXT_BORDER + SPRITE_SIZE * 2, SCREEN_HEIGHT - 9), true,
                        center_left);

            if (gameState.pauseMenuItem == 1) {
                screen.pen = Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b);
            } else {
                screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
            }
            screen.text("Exit", minimal_font, Point(SCREEN_WIDTH - TEXT_BORDER - SPRITE_SIZE * 2, SCREEN_HEIGHT - 9),
                        true, center_right);

            /*if (textFlashTimer < TEXT_FLASH_TIME * 0.6f) {
                screen.text(messageStrings[3][gameSaveData.inputType], minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, TextAlign::center_center);
            }*/
        } else if (gameState.cameraIntro) {
            // Level <num> message
            background_rect(0);
            screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
            screen.text("Level " + std::to_string(gameState.currentLevelNumber + 1), minimal_font, Point(SCREEN_MID_WIDTH, 10),
                        true, center_center);

            // Press <key> to skip intro message
            background_rect(1);
            if (gameState.textFlashTimer < TEXT_FLASH_TIME * 0.6f) {
                screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
                screen.text(messageStrings[1][gameState.gameSaveData.inputType], minimal_font,
                            Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, center_center);
            }
        } else {
            // render hud
            {
                screen.pen = Pen(hudBackground.r, hudBackground.g, hudBackground.b, hudBackground.a);
                screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SPRITE_HALF * 2 + 2));

                screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);

                // Player health
                for (uint8_t i = 0; i < PLAYER_MAX_HEALTH; i++) {
                    if (i < gameState.player.health) {
                        render_sprite(TILE_ID_HEART, Point(2 + i * SPRITE_SIZE, 2));
                        //screen.sprite(TILE_ID_HEART, Point(2 + i * SPRITE_SIZE, 2));
                    } else {
                        render_sprite(TILE_ID_HEART + 1, Point(2 + i * SPRITE_SIZE, 2));
                        //screen.sprite(TILE_ID_HEART + 1, Point(2 + i * SPRITE_SIZE, 2));
                    }
                }

                // Player score
                screen.text(std::to_string(gameState.player.score), minimal_font, Point(SCREEN_WIDTH - SPRITE_SIZE - 2, 2), true,
                            top_right);

                render_sprite(TILE_ID_HUD_COINS, Point(SCREEN_WIDTH - SPRITE_SIZE, 2));
                //screen.sprite(TILE_ID_HUD_COINS, Point(SCREEN_WIDTH - SPRITE_SIZE, 2));

                // Player lives
                screen.text(std::to_string(gameState.player.lives), minimal_font, Point(2 + 6 * SPRITE_SIZE - 2, 2), true,
                            top_right);

                render_sprite(TILE_ID_HUD_LIVES + gameState.playerSelected, Point(2 + 6 * SPRITE_SIZE, 2));
                //screen.sprite(TILE_ID_HUD_LIVES + playerSelected, Point(2 + 6 * SPRITE_SIZE, 2));
            }
        }
    }

    void InGame::update() const {
        ButtonStates &buttonStates1 = gameState.buttonStates;
        if (!gameState.gamePaused) {
            // Game isn't paused, update it.

            gameState.player.update(gameState.dt, buttonStates1);

            update_enemies(gameState.dt, buttonStates1);

            update_checkpoint(gameState.dt);

            update_coins(gameState.dt);

            update_projectiles(gameState.dt);

            update_particles(gameState.dt);

            if (gameState.currentLevelNumber == LEVEL_COUNT - 1) {
                update_thankyou(gameState.dt);
                create_confetti(gameState.dt);
            }

                // Need to rework finish
                gameState.finish.update(gameState.dt, buttonStates1);

                if (std::abs(gameState.player.pos.x - gameState.finish.pos.x) < SPRITE_HALF * 3 && std::abs(
                        gameState.player.pos.y - gameState.finish.pos.y) < SPRITE_HALF) {
                    //// lock player to finish
                    //player.pos.x = finish.pos.x;
                    //player.pos.y = finish.pos.y - 1;

                    if (std::abs(gameState.player.pos.x - gameState.finish.pos.x) < SPRITE_HALF) {
                        gameState.player.pos.x = gameState.finish.pos.x;
                        gameState.player.pos.y = gameState.finish.pos.y - 1;
                    } else {
                        if (gameState.player.pos.x < gameState.finish.pos.x) {
                            gameState.player.vel.x +=
                                    std::max(SPRITE_SIZE * 2 - (gameState.finish.pos.x - gameState.player.pos.x), (float) SPRITE_SIZE) * 0.4f;
                        } else {
                            gameState.player.vel.x -=
                                    std::max(SPRITE_SIZE * 2 - (gameState.player.pos.x - gameState.finish.pos.x), (float) SPRITE_SIZE) * 0.4f;
                        }
                    }
                }
        }
        //if (player.pos.x + SPRITE_SIZE > finish.pos.x - SPRITE_SIZE && player.pos.x < finish.pos.x + SPRITE_SIZE * 2 && player.pos.y + SPRITE_SIZE > finish.pos.y - SPRITE_SIZE && player.pos.y < finish.pos.y + SPRITE_SIZE * 2) {
//    // 'pull' player to finish

        //    player.pos.x += (finish.pos.x - player.pos.x) * 4 * dt;
//    player.pos.y += (finish.pos.y - player.pos.y) * 4 * dt;
//}

        if (gameState.transition[0].is_ready_to_open()) {
            if (gameState.pauseMenuItem == 1) {
                // Player exited level
                switchSceneTo(&scene::levelSelect);
            } else if (gameState.player.lives) {
                switchSceneTo(&scene::won);
            } else {
                switchSceneTo(&scene::lost);
            }

            // Unload coin sfx, load select sfx file back into select sfx slot
            gameState.audioHandler.load(0, 0);
        } else if (gameState.transition[0].is_open()) {
            if (gameState.gamePaused) {
                if (gameState.pauseMenuItem == 0) {
                    if (buttonStates1.RIGHT == 2) {
                        gameState.pauseMenuItem = 1;
                        gameState.audioHandler.play(0);
                    }
                } else if (gameState.pauseMenuItem == 1) {
                    if (buttonStates1.LEFT == 2) {
                        gameState.pauseMenuItem = 0;
                        gameState.audioHandler.play(0);
                    }
                }

                if (buttonStates1.A == 2) {
                    gameState.audioHandler.play(0);

                    if (gameState.pauseMenuItem == 0) {
                        // Unpause game
                        gameState.gamePaused = false;

                        // Load coin sfx file into select sfx slot
                        gameState.audioHandler.load(0, 2);
                    } else if (gameState.pauseMenuItem == 1) {
                        // Exit level
                        close_transition();
                    }
                }
            } else {
                if (gameState.cameraIntro) {
                    //camera.linear_to(dt, cameraStartX, cameraStartY, player.pos.x, player.pos.y, CAMERA_PAN_TIME);
                    gameState.camera.linear_to(gameState.dt, gameState.cameraStartX, gameState.cameraStartY, gameState.player.pos.x, gameState.player.pos.y,
                                                 gameState.currentLevelNumber == LEVEL_COUNT - 1 ? CAMERA_PAN_TIME_FINAL_LEVEL
                                                                                                   : CAMERA_PAN_TIME);

                    //camera.ease_to(dt/5, player.pos.x, player.pos.y);

                    if (gameState.player.pos.x == gameState.camera.pos.x && gameState.player.pos.y == gameState.camera.pos.y) {
                        // Completed
                        gameState.cameraIntro = false;
                        gameState.player.locked = false;
                        // Make player immune when spawning?
                        //player.set_immune();
                    }

                    if (buttonStates1.A == 2) {
                        // Skip intro
                        gameState.cameraIntro = false;
                        gameState.cameraRespawn = true; // goes to player faster
                    }
                } else if (gameState.cameraRespawn) {
                    gameState.camera.ease_out_to(gameState.dt, gameState.player.pos);

                    if (std::abs(gameState.player.pos.x - gameState.camera.pos.x) < CAMERA_RESPAWN_LOCK_MIN &&
                        std::abs(gameState.player.pos.x - gameState.camera.pos.x) < CAMERA_RESPAWN_LOCK_MIN) {
                        // for respawns
                        gameState.cameraRespawn = false;
                        gameState.player.locked = false;
                    }
                } else {
                    /*if (freezePlayer) {
                        for (uint8_t i = 0; i < bosses.size(); i++) {
                            if (bosses[i].get_state() == 2 || bosses[i].get_state() == 3 || bosses[i].is_dead()) {
                                foundBoss = true;
                                camera.ease_out_to(dt, bosses[i].x - (bosses[i].x - player.pos.x) / 2, bosses[i].y - (bosses[i].y - player.pos.y) / 2);
                                break;
                            }
                        }
                    }*/

                    // follow the player with the camera
                    gameState.camera.ease_out_to(gameState.dt, gameState.player.pos);

                    // Handle level end
                    if (gameState.player.pos.x + SPRITE_SIZE > gameState.finish.pos.x + 3 &&
                        gameState.player.pos.x < gameState.finish.pos.x + SPRITE_SIZE - 3 && gameState.player.pos.y + SPRITE_SIZE > gameState.finish.pos.y + 4 &&
                        gameState.player.pos.y < gameState.finish.pos.y + SPRITE_SIZE) {
                        close_transition();
                    }


                    // Handle player life
                    if (gameState.player.lives == 0 && gameState.player.particles.size() == 0) {
                        close_transition();
                    }
                }
            }

            // Allow player to toggle pause game (if game is paused, selecting 'resume' also does same thing
            if (buttonStates1.Y == 2) {
                if (gameState.gamePaused) {
                    gameState.audioHandler.play(0);
                    // Load coin sfx into select sfx slot
                    gameState.audioHandler.load(0, 2);
                    gameState.gamePaused = false;
                } else {
                    gameState.gamePaused = true;
                    gameState.pauseMenuItem = 0;
                    // Unload coin sfx, put select sfx back into select sfx slot
                    gameState.audioHandler.load(0, 0);
                    gameState.audioHandler.play(0);
                }
            }
        }
    }
}