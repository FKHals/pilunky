#include "Scene.h"

#include "engine/engine.hpp"
#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


using namespace blit;

namespace scene {

    void Won::onSwitch() const {
        if (gameState.currentLevelNumber == gameState.allPlayerSaveData[gameState.playerSelected].levelReached) {
            // Just completed level for first time
            gameState.allPlayerSaveData[gameState.playerSelected].levelReached = gameState.currentLevelNumber + 1;

            if (gameState.currentLevelNumber % LEVELS_PER_WORLD == LEVELS_PER_WORLD - 1 || gameState.currentLevelNumber == 8
#ifdef PICO_BUILD
                + 1
#endif // PICO_BUILD
                    ) {
                // Level was last level in world or was level 9
                gameState.cameraNewWorld = true;
                gameState.player.locked = true;
                gameState.camera.reset_temp();
            }
        }

        if (gameState.allLevelSaveData[gameState.playerSelected][gameState.currentLevelNumber].score < gameState.player.score) {
            gameState.allLevelSaveData[gameState.playerSelected][gameState.currentLevelNumber].score = gameState.player.score;
        }
        if (gameState.allLevelSaveData[gameState.playerSelected][gameState.currentLevelNumber].enemiesKilled < gameState.player.enemiesKilled) {
            gameState.allLevelSaveData[gameState.playerSelected][gameState.currentLevelNumber].enemiesKilled = gameState.player.enemiesKilled;
        }
        if (gameState.allLevelSaveData[gameState.playerSelected][gameState.currentLevelNumber].time > gameState.player.levelTimer ||
            gameState.allLevelSaveData[gameState.playerSelected][gameState.currentLevelNumber].time == 0.0f) {
            gameState.allLevelSaveData[gameState.playerSelected][gameState.currentLevelNumber].time = gameState.player.levelTimer;
        }

        //save_game_data();
        save_player_data(gameState.playerSelected);
        save_level_data(gameState.playerSelected, gameState.currentLevelNumber);

        open_transition();
    }

    void Won::render() const {
        render_background();

        render_level();

        render_finish();

        render_entities();

        render_particles();

        screen.pen = Pen(gameBackground.r, gameBackground.g, gameBackground.b,
                         hudBackground.a); // use hudBackground.a to make background semi transparent
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)); // repeat to make darker

        background_rect(0);
        background_rect(1);

        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);

        screen.text("Level complete!", minimal_font, Point(SCREEN_MID_WIDTH, 10), true, center_center);

        display_stats(true);

        if (gameState.textFlashTimer < TEXT_FLASH_TIME * 0.6f) {
            screen.text(messageStrings[2][gameState.gameSaveData.inputType], minimal_font,
                        Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, center_center);
        }
    }

    void Won::update() const {
        ButtonStates &buttonStates1 = gameState.buttonStates;
        update_checkpoint(gameState.dt);
        update_coins(gameState.dt);
        gameState.finish.update(gameState.dt, buttonStates1);
        update_projectiles(gameState.dt);
        update_particles(gameState.dt);

        if (gameState.currentLevelNumber == LEVEL_COUNT - 1) {
            update_thankyou(gameState.dt);
            create_confetti(gameState.dt);
        }

        if (gameState.transition[0].is_ready_to_open()) {
            switchSceneTo(&scene::levelSelect);
        } else if (gameState.transition[0].is_open()) {
            if (buttonStates1.A == 2) {
                gameState.audioHandler.play(0);
                close_transition();
            }
        }
    }
}