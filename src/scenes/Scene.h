#ifndef SUPER_SQUARE_BROS_SCENE_H
#define SUPER_SQUARE_BROS_SCENE_H

#include "../GameInput.hpp"
#include "../Camera.hpp"
#include "../LevelObjects.hpp"


/*template<typename T, typename K>
inline bool isType(const K &k) {
    return typeid(T) == typeid(k);
}*/

/*
TODO: Something like that? But the lambdas would require the GameState to be a struct first because otherwise the
 closure needs to be defined granularly.
    struct Scene {
        const std::function<void()> update;
        const std::function<void()> render;
 */

class Scene {
public:
    virtual void update() const = 0;
    virtual void render() const = 0;

    /**
     * This should run once when a switch to this Scene occurs.
     */
    virtual void onSwitch() const = 0;

    [[nodiscard]] virtual const char* id() const = 0;

    // FIXME: This is not a great idea and should be replaced using polymorphism including id etc.
    bool instanceOf(const Scene * scene) const {
        return this->id() == scene->id();
    }
};

// This just declares the scene so that the methods do not have to be retyped over and over
// The preprocessor reads `#SceneName` as `"SceneName"`
// This generates the class declaration as well as a class instance
#define DECLARE_SCENE(SceneName, lowerCaseName) \
class SceneName : public Scene { \
public: \
    void update() const final; \
    void render() const final; \
    void onSwitch() const; \
    const char* id() const final { return #SceneName; }; \
}; \
constexpr SceneName lowerCaseName;

namespace scene {
    DECLARE_SCENE(SgIcon, sgIcon)
    DECLARE_SCENE(InputSelect, inputSelect)
    DECLARE_SCENE(Menu, menu)
    DECLARE_SCENE(Credits, credits)
    DECLARE_SCENE(Settings, settings)
    DECLARE_SCENE(CharacterSelect, characterSelect)
    DECLARE_SCENE(LevelSelect, levelSelect)
    DECLARE_SCENE(InGame, inGame)
    DECLARE_SCENE(Lost, lost)
    DECLARE_SCENE(Won, won)
}

void render_background();
void background_rect(uint8_t position);

void open_transition();
void close_transition();

void display_stats(bool showBadges);

void render_finish();
void render_level();
void render_entities();
void render_particles();
void reset_level_vars();

void update_enemies(float dt, ButtonStates& buttonStates);
void update_checkpoint(float dt);
void update_coins(float dt);
void update_projectiles(float dt);
void update_particles(float dt);

void create_confetti(float dt);
void update_thankyou(float dt);

#endif //SUPER_SQUARE_BROS_SCENE_H
