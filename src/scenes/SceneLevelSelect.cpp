#include "Scene.h"

#include "engine/engine.hpp"
#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


using namespace blit;

namespace scene {

    void LevelSelect::onSwitch() const {
        bool wonLevel = gameState.currentScene->instanceOf(&scene::won);

        // Load level select level
        load_level(LEVEL_SELECT_NUMBER);

        if (gameState.currentLevelNumber != NO_LEVEL_SELECTED) {
            // Must have just attempted a level
            // Place player next to finished level, on right if just finished, on left if failed.
            for (LevelTrigger& levelTrigger : gameState.levelTriggers) {
                if (levelTrigger.levelNumber == gameState.currentLevelNumber) {
                    gameState.playerStartX = wonLevel ? levelTrigger.pos.x + SPRITE_HALF * 3 : levelTrigger.pos.x - SPRITE_HALF * 3;
                    gameState.playerStartY = levelTrigger.pos.y;
                    gameState.player.pos.x = gameState.playerStartX;
                    gameState.player.pos.y = gameState.playerStartY;
                    gameState.camera.pos.x = gameState.player.pos.x;
                    gameState.camera.pos.y = gameState.player.pos.y;
                }
            }
        }
        else {
            // Must have just come from title/menu screen
            for (LevelTrigger& levelTrigger : gameState.levelTriggers) {
                if (levelTrigger.levelNumber == gameState.allPlayerSaveData[gameState.playerSelected].levelReached - 1) {
                    gameState.playerStartX = levelTrigger.pos.x + SPRITE_HALF * 3;
                    gameState.playerStartY = levelTrigger.pos.y;
                    gameState.player.pos.x = gameState.playerStartX;
                    gameState.player.pos.y = gameState.playerStartY;
                    gameState.camera.pos.x = gameState.player.pos.x;
                    gameState.camera.pos.y = gameState.player.pos.y;
                }
            }
        }

        // Make sure player attribute setting is done after load_level call, since player is reassigned in that method
        gameState.player.locked = true;
        reset_level_vars();

        open_transition();

        // Reset currentLevelNumber so no level is pre-selected
        gameState.currentLevelNumber = NO_LEVEL_SELECTED;
    };

    void LevelSelect::render() const {
        render_background();

        render_level();

        render_entities();

        // render level triggers
        for (LevelTrigger &levelTrigger : gameState.levelTriggers) {
            levelTrigger.render(gameState.camera);
        }

        render_particles();

        background_rect(0);

        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        screen.text("Select level", minimal_font, Point(SCREEN_MID_WIDTH, 10), true, center_center);

        // render nearby level info
        for (LevelTrigger &levelTrigger1 : gameState.levelTriggers) {
            if (std::abs(gameState.player.pos.x - levelTrigger1.pos.x) < LEVEL_INFO_MAX_RANGE &&
                std::abs(gameState.player.pos.y - levelTrigger1.pos.y) < LEVEL_INFO_MAX_RANGE) {
                background_rect(1);


                screen.pen = Pen(levelTriggerParticleColours[1].r, levelTriggerParticleColours[1].g,
                                 levelTriggerParticleColours[1].b);

                // Level number
                screen.text("Level " + std::to_string(levelTrigger1.levelNumber + 1), minimal_font,
                            Point(SPRITE_HALF, SCREEN_HEIGHT - 9 - SPRITE_HALF), true, center_left);


                screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);

                if (gameState.allPlayerSaveData[gameState.playerSelected].levelReached < levelTrigger1.levelNumber) {
                    // Level is locked
                    screen.text("Level locked", minimal_font,
                                Point(SCREEN_WIDTH - SPRITE_HALF, SCREEN_HEIGHT - 9 + SPRITE_HALF), true, center_right);
                } else if (gameState.allPlayerSaveData[gameState.playerSelected].levelReached == levelTrigger1.levelNumber) {
                    // Level is unlocked and has not been completed
                    screen.text("No highscores", minimal_font,
                                Point(SCREEN_WIDTH - SPRITE_HALF, SCREEN_HEIGHT - 9 + SPRITE_HALF), true, center_right);
                } else {
                    // Level is unlocked and has been completed

                    if (gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].time == 0.0f) {
                        // If time == 0.0f, something's probably wrong (like no save slot for that level, but still save slot for saveData worked)

                        screen.text("Error loading highscores", minimal_font,
                                    Point(SCREEN_WIDTH - SPRITE_HALF, SCREEN_HEIGHT - 9 + SPRITE_HALF), true,
                                    center_right);
                    } else {
                        screen.text(std::to_string(gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].score),
                                    minimal_font,
                                    Point(SCREEN_WIDTH - SPRITE_HALF * 25, SCREEN_HEIGHT - 9 + SPRITE_HALF), true,
                                    center_right);
                        screen.text(std::to_string(
                                gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].enemiesKilled),
                                    minimal_font,
                                    Point(SCREEN_WIDTH - SPRITE_HALF * 16, SCREEN_HEIGHT - 9 + SPRITE_HALF), true,
                                    center_right);

                        // Trim time to 2dp
                        std::string timeString = std::to_string(
                                gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].time);
                        timeString = timeString.substr(0, timeString.find('.') + 3);
                        screen.text(timeString, minimal_font,
                                    Point(SCREEN_WIDTH - SPRITE_HALF * 4, SCREEN_HEIGHT - 9 + SPRITE_HALF), true,
                                    center_right);

                        uint8_t a, b, c;
                        a = gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].score >=
                            levelTargets[levelTrigger1.levelNumber][0][0] ? 0 :
                            gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].score >=
                            levelTargets[levelTrigger1.levelNumber][0][1] ? 1 : 2;
                        b = gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].enemiesKilled >=
                            levelTargets[levelTrigger1.levelNumber][1][0] ? 0 :
                            gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].enemiesKilled >=
                            levelTargets[levelTrigger1.levelNumber][1][1] ? 1 : 2;
                        c = gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].time <=
                            levelTargetTimes[levelTrigger1.levelNumber][0] ? 0 :
                            gameState.allLevelSaveData[gameState.playerSelected][levelTrigger1.levelNumber].time <=
                            levelTargetTimes[levelTrigger1.levelNumber][1] ? 1 : 2;

                        render_sprite(TILE_ID_GOLD_BADGE + a + 24,
                                      Point(SCREEN_WIDTH - SPRITE_HALF * 24, SCREEN_HEIGHT - 9));
                        render_sprite(TILE_ID_GOLD_BADGE + b + 28,
                                      Point(SCREEN_WIDTH - SPRITE_HALF * 15, SCREEN_HEIGHT - 9));
                        render_sprite(TILE_ID_GOLD_BADGE + c + 32,
                                      Point(SCREEN_WIDTH - SPRITE_HALF * 3, SCREEN_HEIGHT - 9));

                        /*render_sprite(TILE_ID_HUD_COINS, Point(SCREEN_WIDTH - SPRITE_HALF * 28, SCREEN_HEIGHT - 9));
                        render_sprite(TILE_ID_HUD_ENEMIES_KILLED, Point(SCREEN_WIDTH - SPRITE_HALF * 18, SCREEN_HEIGHT - 9));
                        render_sprite(TILE_ID_HUD_TIME_TAKEN, Point(SCREEN_WIDTH - SPRITE_HALF * 5, SCREEN_HEIGHT - 9));*/
                    }
                }

                //screen.text(std::to_string(saveData.scores[levelTriggers[i].levelNumber]), minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, TextAlign::center_center);
            }
        }
    }

    void LevelSelect::update() const {
        ButtonStates &buttonStates1 = gameState.buttonStates;
        gameState.player.update(gameState.dt, buttonStates1);

        update_enemies(gameState.dt, buttonStates1);

        // update level triggers
        {
            for (int i = 0; i < gameState.levelTriggers.size(); i++) {
                gameState.levelTriggers[i].update(gameState.dt, buttonStates1);
                if (!gameState.levelTriggers[i].visible && gameState.levelTriggers[i].particles.size() == 0) {
                    gameState.currentLevelNumber = gameState.levelTriggers[i].levelNumber;
                    gameState.currentWorldNumber = gameState.currentLevelNumber / LEVELS_PER_WORLD;
                }
            }
            gameState.levelTriggers.erase(std::remove_if(gameState.levelTriggers.begin(), gameState.levelTriggers.end(), [](LevelTrigger& levelTrigger) { return (!levelTrigger.visible && levelTrigger.particles.size() == 0); }), gameState.levelTriggers.end());
        }

        update_particles(gameState.dt);

        // Button handling

        if (gameState.transition[0].is_ready_to_open()) {
            if (gameState.menuBack) {
                gameState.menuBack = false;
                switchSceneTo(&scene::characterSelect);
            } else {
                switchSceneTo(&scene::inGame);
            }
        } else if (gameState.transition[0].is_open()) {
            if (gameState.cameraNewWorld) {
                if (gameState.camera.temp.x == 0.0f && gameState.camera.temp.y == 0.0f) {
                    gameState.camera.temp.x = gameState.camera.pos.x;
                    gameState.camera.temp.y = gameState.camera.pos.y;
                }
                gameState.camera.linear_to(gameState.dt, gameState.camera.temp.x, gameState.camera.temp.y,
                                             gameState.levelTriggers[gameState.allPlayerSaveData[gameState.playerSelected].levelReached].pos.x,
                                             gameState.levelTriggers[gameState.allPlayerSaveData[gameState.playerSelected].levelReached].pos.y,
                                             CAMERA_NEW_WORLD_TIME);

                if (gameState.levelTriggers[gameState.allPlayerSaveData[gameState.playerSelected].levelReached].pos.x == gameState.camera.pos.x &&
                    gameState.levelTriggers[gameState.allPlayerSaveData[gameState.playerSelected].levelReached].pos.y == gameState.camera.pos.y) {
                    if (gameState.camera.timer_started()) {
                        gameState.camera.update_timer(gameState.dt);
                        if (gameState.camera.get_timer() > NEW_WORLD_DELAY_TIME) {
                            // Animation finished
                            gameState.cameraNewWorld = false;
                            gameState.camera.reset_temp();
                            gameState.camera.reset_timer();
                        }
                    } else {
                        gameState.camera.start_timer();
                    }
                }
            } else if (gameState.cameraRespawn) {
                gameState.camera.ease_out_to(gameState.dt, gameState.player.pos);

                if (std::abs(gameState.player.pos.x - gameState.camera.pos.x) < CAMERA_RESPAWN_LOCK_MIN &&
                    std::abs(gameState.player.pos.x - gameState.camera.pos.x) < CAMERA_RESPAWN_LOCK_MIN) { //TODO Why?
                    // for respawns
                    gameState.cameraRespawn = false;
                    gameState.player.locked = false;

                    // to stop player completely dying
                    gameState.player.lives = 3;
                }
            } else {
                gameState.camera.ease_out_to(gameState.dt, gameState.player.pos);

                gameState.player.locked = false;
            }


            if (gameState.currentLevelNumber != NO_LEVEL_SELECTED) {
                close_transition();
                gameState.player.locked = true;
            } else if (buttonStates1.Y == 2) {
                gameState.audioHandler.play(0);

                gameState.menuBack = true;
                close_transition();
                gameState.player.locked = true;
            }
        }
    }
}