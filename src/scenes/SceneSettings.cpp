#include "Scene.h"

#include "engine/engine.hpp"
#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


#ifdef PICO_BUILD
const uint8_t MAX_HACKY_FAST_MODE = 2;
#endif // PICO_BUILD

using namespace blit;

namespace scene {

    void Settings::onSwitch() const {
        // do nothing
    }

    void Settings::render() const {
        render_background();

        render_level();

        render_entities();

        /*screen.pen = Pen(hudBackground.r, hudBackground.g, hudBackground.b, hudBackground.a);
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));*/

        screen.pen = Pen(gameBackground.r, gameBackground.g, gameBackground.b,
                         hudBackground.a); // use hudBackground.a to make background semi transparent
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
        //screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)); // repeat to make darker

        background_rect(0);

        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        screen.text("Settings", minimal_font, Point(SCREEN_MID_WIDTH, 10), true, center_center);

        screen.text("Checkpoints:", minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT - SPRITE_SIZE * 3), true,
                    center_left);
        screen.text("Music:", minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT - SPRITE_SIZE), true, center_left);
        screen.text("SFX:", minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_SIZE), true, center_left);

#ifdef PICO_BUILD
        screen.text("Hacky fast mode:", minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_SIZE * 3), true, TextAlign::center_left);
#endif // PICO_BUILD

        if (gameState.settingsItem == 0) {
            screen.pen = Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b);
        }
        screen.text(gameState.gameSaveData.checkpoints ? "On" : "Off", minimal_font,
                    Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_MID_HEIGHT - SPRITE_SIZE * 3), true, center_right);

        if (gameState.settingsItem == 1) {
            screen.pen = Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b);
        } else {
            screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        }
        screen.text(gameState.gameSaveData.musicVolume ? "On" : "Off", minimal_font,
                    Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_MID_HEIGHT - SPRITE_SIZE), true, center_right);

        if (gameState.settingsItem == 2) {
            screen.pen = Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b);
        } else {
            screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        }
        screen.text(gameState.gameSaveData.sfxVolume ? "On" : "Off", minimal_font,
                    Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_SIZE), true, center_right);

#ifdef PICO_BUILD
        if (gameState.settingsItem == 3) {
                screen.pen = Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b);
            }
            else {
                screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
            }
            screen.text(gameState.gameSaveData.hackyFastMode ?
                std::to_string(gameState.gameSaveData.hackyFastMode) :
                "Off", minimal_font, Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_SIZE * 3), true, TextAlign::center_right
            );
#endif // PICO_BUILD

        // Press <key> to go back
        background_rect(1);

        /*if (textFlashTimer < TEXT_FLASH_TIME * 0.6f) {
            screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
            screen.text(messageStrings[4][gameSaveData.inputType], minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, TextAlign::center_center);
        }*/
        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        screen.text(messageStrings[5][gameState.gameSaveData.inputType], minimal_font, Point(TEXT_BORDER, SCREEN_HEIGHT - 9),
                    true, center_left);
        screen.text(messageStrings[6][gameState.gameSaveData.inputType], minimal_font,
                    Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_HEIGHT - 9), true, center_right);
    }

    void Settings::update() const {
        update_coins(gameState.dt);
        update_checkpoint(gameState.dt);

        if (gameState.transition[0].is_ready_to_open()) {
            if (gameState.menuBack) {
                gameState.menuBack = false;
                switchSceneTo(&scene::menu);
            }
            /*else {
                if (menuItem == 0) {
                    start_character_select();
                }
                else {
                    start_settings();
                }
            }*/
        } else if (gameState.transition[0].is_open()) {
            if (gameState.buttonStates.A == 2) {
                gameState.audioHandler.play(0);

                if (gameState.settingsItem == 0) {
                    gameState.gameSaveData.checkpoints = !gameState.gameSaveData.checkpoints;
                } else if (gameState.settingsItem == 1) {
                    gameState.gameSaveData.musicVolume = !gameState.gameSaveData.musicVolume;
                    gameState.audioHandler.set_volume(7, gameState.gameSaveData.musicVolume ? DEFAULT_VOLUME : 0);
                } else if (gameState.settingsItem == 2) {
                    gameState.gameSaveData.sfxVolume = !gameState.gameSaveData.sfxVolume;
                    for (uint8_t i = 0; i < 7; i++) {
                        gameState.audioHandler.set_volume(i, gameState.gameSaveData.sfxVolume ? DEFAULT_VOLUME : 0);
                    }
                }
#ifdef PICO_BUILD
                else if (gameState.settingsItem == 3) {
                    if (gameState.gameSaveData.hackyFastMode < MAX_HACKY_FAST_MODE) {
                        gameState.gameSaveData.hackyFastMode++;
                    }
                    else {
                        gameState.gameSaveData.hackyFastMode = 0;
                    }
                }

            }
            else if (gameState.buttonStates.LEFT == 2) {
                if (gameState.settingsItem == 3) {
                    if (gameState.gameSaveData.hackyFastMode > 0) {
                        gameState.gameSaveData.hackyFastMode--;
                    }
                }
            }
            else if (gameState.buttonStates.RIGHT == 2) {
                if (gameState.settingsItem == 3) {
                    if (gameState.gameSaveData.hackyFastMode < MAX_HACKY_FAST_MODE) {
                        gameState.gameSaveData.hackyFastMode++;
                    }
                }
#endif // PICO_BUILD
            } else if (gameState.buttonStates.Y == 2) {
                // Exit settings
                gameState.audioHandler.play(0);

                // Save settings data
                save_game_data();

                gameState.menuBack = true;
                close_transition();
            } else if (gameState.buttonStates.UP == 2 && gameState.settingsItem > 0) {
                gameState.settingsItem--;
                gameState.audioHandler.play(0);
            } else if (gameState.buttonStates.DOWN == 2 && gameState.settingsItem < SETTINGS_COUNT) {
                gameState.settingsItem++;
                gameState.audioHandler.play(0);
            }
        }
    }
}