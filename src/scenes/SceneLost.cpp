#include "Scene.h"

#include "engine/engine.hpp"
#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


using namespace blit;

namespace scene {

    void Lost::onSwitch() const {
        open_transition();
    }

    void Lost::render() const {
        render_background();

        render_level();

        render_finish();

        render_entities();

        render_particles();

        screen.pen = Pen(gameBackground.r, gameBackground.g, gameBackground.b,
                         hudBackground.a); // use hudBackground.a to make background semi transparent
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)); // repeat to make darker

        background_rect(0);
        background_rect(1);

        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);

        screen.text("Level failed.", minimal_font, Point(SCREEN_MID_WIDTH, 10), true, center_center);

        display_stats(false);

        if (gameState.textFlashTimer < TEXT_FLASH_TIME * 0.6f) {
            screen.text(messageStrings[2][gameState.gameSaveData.inputType], minimal_font,
                        Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, center_center);
        }
    }

    void Lost::update() const {
        ButtonStates &buttonStates1 = gameState.buttonStates;
        update_checkpoint(gameState.dt);
        update_coins(gameState.dt);
        gameState.finish.update(gameState.dt, buttonStates1);
        update_projectiles(gameState.dt);
        update_particles(gameState.dt);

        if (gameState.currentLevelNumber == LEVEL_COUNT - 1) {
            update_thankyou(gameState.dt);
            create_confetti(gameState.dt);
        }

        if (gameState.transition[0].is_ready_to_open()) {
            switchSceneTo(&scene::levelSelect);
        } else if (gameState.transition[0].is_open()) {
            if (buttonStates1.A == 2) {
                gameState.audioHandler.play(0);
                close_transition();
            }
        }
    }
}