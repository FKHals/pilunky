#include "Scene.h"

#include "engine/save.hpp"
#include "engine/engine.hpp"
#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


using namespace blit;


const uint16_t SG_ICON_INDEX = 128;
const uint8_t SG_ICON_SIZE = SPRITE_SIZE * 4;

extern Colour splashColour;
extern ButtonStates buttonStates;
extern PlayerSaveData allPlayerSaveData[2];
extern LevelSaveData allLevelSaveData[2][LEVEL_COUNT];
extern GameSaveData gameSaveData;
extern GameVersion gameVersion;
extern AudioHandler::AudioHandler audioHandler;


namespace scene {

    void SgIcon::onSwitch() const {
        // do nothing
    }

    void SgIcon::render() const {
        // Render splash screen background instead of normal blue background
        screen.pen = Pen(gameState.splashColour.r, gameState.splashColour.g, gameState.splashColour.b);
        screen.clear();

        screen.stretch_blit(screen.sprites, Rect((SG_ICON_INDEX % 16) * SPRITE_SIZE, (SG_ICON_INDEX / 16) * SPRITE_SIZE, SG_ICON_SIZE, SG_ICON_SIZE), Rect(SCREEN_MID_WIDTH - SG_ICON_SIZE, SCREEN_MID_HEIGHT - SG_ICON_SIZE, SG_ICON_SIZE * 2, SG_ICON_SIZE * 2));

#ifdef PICO_BUILD
        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);

        //screen.text("itch.io:", minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - SPRITE_SIZE * 2.5f), true, TextAlign::center_center);
        //screen.text("Scorpion Games UK", minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - SPRITE_SIZE), true, TextAlign::center_center);
        screen.text("scorpion", minimal_font, Point(0, SCREEN_HEIGHT - SPRITE_HALF * 3), true, TextAlign::center_left);
        screen.text("-", minimal_font, Point(43, SCREEN_HEIGHT - SPRITE_HALF * 3), true, TextAlign::center_left);
        screen.text("games", minimal_font, Point(46, SCREEN_HEIGHT - SPRITE_HALF * 3), true, TextAlign::center_left);
        screen.text("-", minimal_font, Point(73, SCREEN_HEIGHT - SPRITE_HALF * 3), true, TextAlign::center_left);
        screen.text("uk", minimal_font, Point(76, SCREEN_HEIGHT - SPRITE_HALF * 3), true, TextAlign::center_left);
        screen.text(".itch.io", minimal_font, Point(87, SCREEN_HEIGHT - SPRITE_HALF * 3), true, TextAlign::center_left);
#endif // PICO_BUILD
    }

    void SgIcon::update() const {
        if (gameState.splashColour.a == 255) {
            // Init game
            switchSceneTo(&scene::inputSelect);
            // init game
            {
                bool success = read_save(gameState.gameSaveData);

                // Load save data
                // Attempt to load the first save slot.
                if (success) {
                    bool reset = false;

                    if (get_version_struct(gameState.gameSaveData.version).major != gameState.gameVersion.major) {
                        reset = true;
                    }
#ifdef RESET_SAVE_DATA_IF_MINOR_DIFF
                    else if (get_version_struct(gameSaveData.version).minor != gameVersion.minor) {
                        reset = true;
                    }
#endif
                    if (reset) {
                        GameVersion saveDataVersion = get_version_struct(gameState.gameSaveData.version);

                        printf("Warning: Saved game data is out of date, save version is %d (v%d.%d.%d), but firmware version is %d (v%d.%d.%d)\n",
                               gameState.gameSaveData.version, saveDataVersion.major, saveDataVersion.minor,
                               saveDataVersion.build, get_version(gameState.gameVersion), gameState.gameVersion.major, gameState.gameVersion.minor,
                               gameState.gameVersion.build);
                        printf("Resetting save data...\n");

                        success = false;
                        // reset save
                        {
                            gameState.gameSaveData.version = get_version(gameState.gameVersion);
                            gameState.gameSaveData.inputType = CONTROLLER;
                            gameState.gameSaveData.checkpoints = false;
                            gameState.gameSaveData.musicVolume = true;
                            gameState.gameSaveData.sfxVolume = true;
                            gameState.gameSaveData.hackyFastMode = 0;
                            save_game_data();

                            gameState.allPlayerSaveData[0].levelReached = 0;
                            save_player_data(0);

                            gameState.allPlayerSaveData[1].levelReached = 0;
                            save_player_data(1);

                            for (uint8_t i = 0; i < LEVEL_COUNT; i++) {
                                gameState.allLevelSaveData[0][i].score = 0;
                                gameState.allLevelSaveData[0][i].enemiesKilled = 0;
                                gameState.allLevelSaveData[0][i].time = 0.0f;
                                save_level_data(0, i);

                                gameState.allLevelSaveData[1][i].score = 0;
                                gameState.allLevelSaveData[1][i].enemiesKilled = 0;
                                gameState.allLevelSaveData[1][i].time = 0.0f;
                                save_level_data(1, i);
                            }
                        }
                    }
                }

                if (success) {
                    GameVersion saveDataVersion = get_version_struct(gameState.gameSaveData.version);
                    printf("Save data loaded, save version: %d (v%d.%d.%d)\n", gameState.gameSaveData.version,
                           saveDataVersion.major, saveDataVersion.minor, saveDataVersion.build);

                    // Loaded sucessfully!
                    gameState.currentScene = &scene::menu;

                    // Load menu level
                    load_level(LEVEL_COUNT);

                    // Setup audio volumes (for some reason it isn't being loaded properly??)
                    gameState.audioHandler.set_volume(gameState.gameSaveData.sfxVolume ? DEFAULT_VOLUME : 0);
                    gameState.audioHandler.set_volume(7, gameState.gameSaveData.musicVolume ? DEFAULT_VOLUME : 0);

                } else {
                    // No save file or it failed to load, set up some defaults.
                    // Should I use reset_save here?
                    gameState.gameSaveData.version = get_version(gameState.gameVersion);

                    gameState.gameSaveData.inputType = CONTROLLER;

                    gameState.gameSaveData.checkpoints = false;
                    gameState.gameSaveData.musicVolume = true;
                    gameState.gameSaveData.sfxVolume = true;

                    gameState.gameSaveData.hackyFastMode = 0;

                    // gameState is by default set to STATE_INPUT_SELECT

#if defined(TARGET_32BLIT_HW) || defined(PICO_BUILD)
                    // If it's a 32blit, don't bother asking
                    gameState.currentScene = &scene::menu;

                    // Load menu level
                    load_level(LEVEL_COUNT);

                    // Save inputType
                    save_game_data();
#endif // TARGET_32BLIT_HW || PICO_BUILD
                }
            }
        } else if (gameState.splashColour.a > 0) {
            if (gameState.splashColour.a <= 255 - FADE_STEP) {
                gameState.splashColour.a += FADE_STEP;
            } else {
                gameState.splashColour.a = 255;
            }
        } else {
            if (gameState.buttonStates.A || gameState.buttonStates.B || gameState.buttonStates.X || gameState.buttonStates.Y || !gameState.audioHandler.is_playing(7)) {
                gameState.splashColour.a = FADE_STEP;
            }
        }
    }
}