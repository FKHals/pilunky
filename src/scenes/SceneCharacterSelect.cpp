#include "Scene.h"

#include "engine/engine.hpp"
#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


using namespace blit;

namespace scene {

    void CharacterSelect::onSwitch() const {
        // Load character select level
        load_level(LEVEL_COUNT + 1);

        if (gameState.playerSelected) {
            gameState.player = Player(gameState.playerStartX + SPRITE_SIZE * 7, gameState.playerStartY, 1);
        }
        else {
            gameState.player = Player(gameState.playerStartX, gameState.playerStartY, 0);
        }

        open_transition();
    }

    void CharacterSelect::render() const {
        render_background();

        render_level();

        render_entities();

        if (gameState.playerSelected) {
            render_sprite(TILE_ID_PLAYER_1, Point(SCREEN_MID_WIDTH + gameState.playerStartX - gameState.camera.pos.x,
                                                  SCREEN_MID_HEIGHT + gameState.playerStartY - gameState.camera.pos.y), HORIZONTAL);
            //screen.sprite(TILE_ID_PLAYER_1, Point(SCREEN_MID_WIDTH + playerStartX - camera.pos.x, SCREEN_MID_HEIGHT + playerStartY - camera.pos.y), SpriteTransform::HORIZONTAL);
        } else {
            render_sprite(TILE_ID_PLAYER_2, Point(SCREEN_MID_WIDTH + gameState.playerStartX - gameState.camera.pos.x + SPRITE_SIZE * 7,
                                                  SCREEN_MID_HEIGHT + gameState.playerStartY - gameState.camera.pos.y));
            //screen.sprite(TILE_ID_PLAYER_2, Point(SCREEN_MID_WIDTH + playerStartX - camera.pos.x + SPRITE_SIZE * 7, SCREEN_MID_HEIGHT + playerStartY - camera.pos.y));
        }

        background_rect(0);
        background_rect(1);

        screen.pen = Pen(hudBackground.r, hudBackground.g, hudBackground.b, hudBackground.a);
        screen.rectangle(Rect(0, SCREEN_HEIGHT - (SPRITE_SIZE + 12 + 12), SCREEN_WIDTH, 12));

        screen.pen = Pen(levelTriggerParticleColours[1].r, levelTriggerParticleColours[1].g,
                         levelTriggerParticleColours[1].b);
        screen.text(
                "Player " + std::to_string(gameState.playerSelected + 1) + " (Save " + std::to_string(
                        gameState.playerSelected + 1) + ")",
                minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 10 - 12), true, center_center);

        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        screen.text("Select Player", minimal_font, Point(SCREEN_MID_WIDTH, 10), true, center_center);

        if (gameState.textFlashTimer < TEXT_FLASH_TIME * 0.6f) {
            screen.text(messageStrings[0][gameState.gameSaveData.inputType], minimal_font,
                        Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, center_center);
        }
    }

    void CharacterSelect::update() const {
        // Dummy states is used to make selected player continually jump (sending A key pressed).
        ButtonStates dummyStates = {0};
        dummyStates.A = 2;
        gameState.player.update(gameState.dt, dummyStates);

        if (gameState.transition[0].is_ready_to_open()) {
            if (gameState.menuBack) {
                gameState.menuBack = false;
                switchSceneTo(&scene::menu);
            } else {
                switchSceneTo(&scene::levelSelect);
            }
        } else if (gameState.transition[0].is_open()) {
            if (gameState.buttonStates.RIGHT && !gameState.playerSelected) {
                gameState.audioHandler.play(0);

                gameState.playerSelected = 1;
                gameState.player = Player(gameState.playerStartX + SPRITE_SIZE * 7, gameState.playerStartY, 1);
                gameState.player.lastDirection = 0;
            } else if (gameState.buttonStates.LEFT && gameState.playerSelected) {
                gameState.audioHandler.play(0);

                gameState.playerSelected = 0;
                gameState.player = Player(gameState.playerStartX, gameState.playerStartY, 0);
                gameState.player.lastDirection = 1;
            }

            if (gameState.buttonStates.A == 2) {
                gameState.audioHandler.play(0);

                close_transition();
            } else if (gameState.buttonStates.Y == 2) {
                gameState.audioHandler.play(0);

                gameState.menuBack = true;
                close_transition();
            }
        }
    }
}