#ifndef SUPER_SQUARE_BROS_GAMESTATE_H
#define SUPER_SQUARE_BROS_GAMESTATE_H

#include "engine/api.hpp"
#include "engine/engine.hpp"
#include "engine/version.hpp"

#include "Camera.hpp"
#include "GameInput.hpp"
#include "Constants.hpp"
#include "Effects.hpp"
#include "LevelObjects.hpp"
#include "entities/Entity.hpp"
#include "scenes/Scene.h"
#include "Data.hpp"
#include "audio/Audio.hpp"
#include "Tiles.hpp"


#define MAX_MAP_WIDTH 256
#define MAX_MAP_HEIGHT 32

using namespace blit;


class GameState {
public:
    uint16_t levelDeathBoundary;
    float dt;
    uint32_t lastTime;
    AudioHandler::AudioHandler audioHandler;
    bool menuBack;  // tells menu to go backwards instead of forwards.
    bool gamePaused;  // used for determining if game is paused or not.
    bool coinSfxAlternator;  // used for alternating channel used for coin pickup sfx
    bool cameraIntro;
    bool cameraRespawn;
    bool cameraNewWorld;
    uint16_t cameraStartX;
    uint16_t cameraStartY;
    uint16_t playerStartX;
    uint16_t playerStartY;
    float textFlashTimer;
    uint8_t playerSelected;
    uint8_t pauseMenuItem;
    uint8_t menuItem;
    uint8_t settingsItem;
    uint8_t creditsItem;
    float snowGenTimer;
    float confettiGenTimer;
    bool slowPlayer;
    bool dropPlayer;
    bool repelPlayer;
    bool bossBattle;
    float thankyouValue;
    uint8_t currentLevelNumber;
    uint8_t currentWorldNumber;
    GameVersion gameVersion;
    // For metadata
    GameMetadata metadata;
    Scene const * currentScene;
    ButtonStates buttonStates;
    GameSaveData gameSaveData;
    PlayerSaveData allPlayerSaveData[2];
    LevelSaveData allLevelSaveData[2][LEVEL_COUNT];
    LevelData levelData;
    ScreenShake shaker;
    Colour splashColour;
    Camera camera;
    std::vector<ImageParticle> imageParticles;
    std::vector<Projectile> projectiles;
    std::vector<Coin> coins;
    Finish finish;
    AnimatedTransition transition[SCREEN_TILE_SIZE];
    Checkpoint checkpoint;
    std::vector<LevelTrigger> levelTriggers;
    std::vector<Enemy> enemies;
    Player player;

    GameState();
    Tile getMapTile(uint16_t x_pos, uint16_t y_pos);
    void setMapTile(uint16_t x_pos, uint16_t y_pos, Tile tile);
private:
    Tile map[MAX_MAP_HEIGHT][MAX_MAP_WIDTH];
};

extern GameState gameState;

void switchSceneTo(Scene const * nextScene);

#endif //SUPER_SQUARE_BROS_GAMESTATE_H
