#include "Projectile.hpp"

#include "engine/api.hpp"


using namespace blit;


Projectile::Projectile() {
    pos = blit::Vec2{0, 0};
    vel = blit::Vec2{0, 0};

    id = 0;

    gravity = false;

    width = SPRITE_HALF;
}

Projectile::Projectile(float xPosition, float yPosition,
                       float xVelocity, float yVelocity,
                       uint16_t tileId, bool gravity, uint8_t rectWidth) {
    this->pos.x = xPosition;
    this->pos.y = yPosition;
    this->vel.x = xVelocity;
    this->vel.y = yVelocity;
    id = tileId;
    this->gravity = gravity;
    width = rectWidth;
}

void Projectile::update(float dt, ButtonStates& buttonStates) {
    if (gravity) {
        // Update gravity
        vel.y += PROJECTILE_GRAVITY * dt;
        vel.y = std::min(vel.y, (float)PROJECTILE_GRAVITY_MAX);
    }

    // Move entity y
    pos.y += vel.y * dt;

    // Move entity x
    pos.x += vel.x * dt;
}

void Projectile::render(Camera& camera) {
    render_sprite(id, Point(SCREEN_MID_WIDTH + pos.x - camera.pos.x, SCREEN_MID_HEIGHT + pos.y - camera.pos.y));
    //screen.sprite(id, Point(SCREEN_MID_WIDTH + x - camera.pos.x - SPRITE_QUARTER, SCREEN_MID_HEIGHT + y - camera.pos.y - SPRITE_QUARTER));
    //screen.rectangle(Rect(SCREEN_MID_WIDTH + x - camera.pos.x, SCREEN_MID_HEIGHT + y - camera.pos.y, 4, 4));
}

bool Projectile::is_colliding(float playerX, float playerY) {
    return pos.x + SPRITE_HALF + width / 2 > playerX
           && pos.x + SPRITE_HALF - width / 2 < playerX + SPRITE_SIZE
           && pos.y + SPRITE_HALF + width / 2 > playerY
           && pos.y + SPRITE_HALF - width / 2 < playerY + SPRITE_SIZE;
}