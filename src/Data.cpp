#include "Data.hpp"

#include "engine/save.hpp"

#include "Constants.hpp"
#include "LevelObjects.hpp"
#include "Projectile.hpp"
#include "audio/Audio.hpp"
#include "entities/Entity.hpp"
#include "GameState.hpp"
#include "Tiles.hpp"

// auto generated assets
#include "assets.hpp"


using namespace blit;


uint16_t get_version(GameVersion gameVersionData) {
    // return VERSION_MAJOR * 256 + VERSION_MINOR * 16 + VERSION_BUILD;
    return gameVersionData.major * 256 + gameVersionData.minor * 16 + gameVersionData.build;
}

GameVersion get_version_struct(uint16_t version) {
    GameVersion gameVersionData{};

    gameVersionData.major = version / 256;
    version %= 256;

    gameVersionData.minor = version / 16;
    version %= 16;

    gameVersionData.build = version;

    return gameVersionData;
}


void save_game_data() {
    // Write save data
    write_save(gameState.gameSaveData);
}

void save_level_data(uint8_t playerID, uint8_t levelNumber) {
    // Write level data
    write_save(gameState.allLevelSaveData[playerID][levelNumber], (playerID * (BYTE_SIZE + 1)) + 1 + levelNumber + 1);
}

void save_player_data(uint8_t playerID) {
    // Write level data
    write_save(gameState.allPlayerSaveData[playerID], (playerID * (BYTE_SIZE + 1)) + 1);
}

LevelSaveData load_level_data(uint8_t playerID, uint8_t levelNumber) {
    LevelSaveData levelSaveData;
    if (read_save(levelSaveData, (playerID * (BYTE_SIZE + 1)) + 1 + levelNumber + 1)) {
        // Success
    }
    else {
        // Set some defaults
        levelSaveData.score = 0;
        levelSaveData.enemiesKilled = 0;
        levelSaveData.time = 0.0f; // If time == 0.0f, game realises that it's a N/A time
    }
    return levelSaveData;
}

PlayerSaveData load_player_data(uint8_t playerID) {
    PlayerSaveData playerSaveData;
    if (read_save(playerSaveData, (playerID * (BYTE_SIZE + 1)) + 1)) {
        // Success
    }
    else {
        // Set some defaults
        playerSaveData.levelReached = 0;
    }
    return playerSaveData;
}

// struct to handle level data header...
// this will probably need to be revisited/reworked if 32blit-tools has *any* kind of update to it...
#pragma pack(push,1)
struct TMX16 {
    char head[4];
    uint16_t header_length;
    uint16_t flags;
    uint16_t empty_tile;
    uint16_t width;
    uint16_t height;
    uint16_t layers;
    uint16_t data[];
};
#pragma pack(pop)

const float LEVEL_DEATH_BOUNDARY_SCALE = 1.5f;

const uint16_t SNOW_LEVEL_INIT_COUNT = 300;
const uint16_t SNOW_LEVEL_SELECT_INIT_COUNT = 200;

const uint16_t CONFETTI_INIT_COUNT = 400;

const std::vector<uint16_t> coinFrames = { TILE_ID_COIN, TILE_ID_COIN + 1, TILE_ID_COIN + 2, TILE_ID_COIN + 3, TILE_ID_COIN + 2, TILE_ID_COIN + 1 };
const std::vector<uint16_t> finishFrames = { TILE_ID_FINISH, TILE_ID_FINISH + 1, TILE_ID_FINISH + 2, TILE_ID_FINISH + 3, TILE_ID_FINISH + 4, TILE_ID_FINISH + 5 };

const uint8_t* asset_levels[] = {
        asset_level0,
        asset_level1,
        asset_level2,
        asset_level3,
        asset_level4,
        asset_level5,
        asset_level6,
        asset_level7,
        asset_level8,
        asset_level9,
#ifdef PICO_BUILD
        asset_level10,
#endif // PICO_BUILD
        asset_level_title,
        asset_level_char_select,
        asset_level_level_select
};

void load_level(uint8_t levelNumber) {
    gameState.snowGenTimer = 0.0f;
    gameState.confettiGenTimer = 0.0f;

    // Variables for finding start and finish positions
    uint16_t finishX, finishY;
    uint16_t checkpointX, checkpointY;

    gameState.playerStartX = gameState.playerStartY = 0;
    gameState.cameraStartX = gameState.cameraStartY = 0;
    finishX = finishY = 0;
    checkpointX = checkpointY = 0;


    // Get a pointer to the map header
    TMX16* tmx = (TMX16*)asset_levels[levelNumber];

    uint16_t levelWidth = tmx->width;
    uint16_t levelHeight = tmx->height;
    uint32_t levelSize = levelWidth * levelHeight;

    gameState.levelData.levelWidth = levelWidth;
    gameState.levelData.levelHeight = levelHeight;

    gameState.levelDeathBoundary = gameState.levelData.levelHeight * SPRITE_SIZE * LEVEL_DEATH_BOUNDARY_SCALE;

    // fill map with emptiness
    for (uint16_t y = 0; y < levelHeight; y++) {
        for (uint16_t x = 0; x < levelWidth; x++) {
            gameState.setMapTile(x, y, Tile::EMPTY);
        }
    }

    gameState.coins.clear();
    gameState.enemies.clear();
    gameState.levelTriggers.clear();
    gameState.projectiles.clear();
    gameState.imageParticles.clear();

    // Foreground Layer
    for (uint32_t i = 0; i < levelSize; i++) {
        if (tmx->data[i] == TILE_ID_EMPTY) {
            // Is a blank tile, don't do anything
        }
        else if (tmx->data[i] == TILE_ID_COIN) {
            gameState.coins.push_back(Coin((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, coinFrames));
        }
        else {

            uint16_t x = i % levelWidth;
            uint16_t y = i / levelWidth;

            gameState.setMapTile(x, y, Tile::SOLID);
        }
    }

    // Entity Spawns Layer
    for (uint32_t i = 0; i < levelSize; i++) {
        uint32_t index = i + levelSize;

        if (tmx->data[index] == TILE_ID_EMPTY) {
            // Is a blank tile, don't do anything
        }
        else if (tmx->data[index] == TILE_ID_PLAYER_1) {
            gameState.playerStartX = (i % levelWidth) * SPRITE_SIZE;
            gameState.playerStartY = (i / levelWidth) * SPRITE_SIZE;
        }
        else if (tmx->data[index] == TILE_ID_CAMERA) {
            gameState.cameraStartX = (i % levelWidth) * SPRITE_SIZE;
            gameState.cameraStartY = (i / levelWidth) * SPRITE_SIZE;
        }
        else if (tmx->data[index] == TILE_ID_FINISH) {
            finishX = (i % levelWidth) * SPRITE_SIZE;
            finishY = (i / levelWidth) * SPRITE_SIZE;
        }
        else if (tmx->data[index] == TILE_ID_CHECKPOINT) {
            if (gameState.gameSaveData.checkpoints) {
                checkpointX = (i % levelWidth) * SPRITE_SIZE;
                checkpointY = (i / levelWidth) * SPRITE_SIZE;
            }
        }
        else if (tmx->data[index] == TILE_ID_LEVEL_TRIGGER) {
            gameState.levelTriggers.push_back(LevelTrigger((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, 0));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_1) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[0], 0));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_2) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[1], 1));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_3) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[2], 2));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_4) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[3], 3));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_5) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[4], 4));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_6) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[5], 5));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_7) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[6], 6));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_8) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[7], 7));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_9) {
            // A ninth enemy!?
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[8], 8));
        }
        /*
        else if (tmx->data[index] == TILE_ID_SPIKE_BOTTOM ||
                 tmx->data[index] == TILE_ID_SPIKE_TOP ||
                 tmx->data[index] == TILE_ID_SPIKE_LEFT ||
                 tmx->data[index] == TILE_ID_SPIKE_RIGHT) {

            gameState.spikes.push_back(Tile((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, tmx->data[index]));
        }
         */
    }

    // Sort levelTriggers by x and relabel
    std::sort(gameState.levelTriggers.begin(), gameState.levelTriggers.end(),
            // Function used for vector sorting
              [](const LevelTrigger& a, const LevelTrigger& b) -> bool { return a.pos.x < b.pos.x; });
    for (uint8_t i = 0; i < gameState.levelTriggers.size(); i++) {
        gameState.levelTriggers[i].levelNumber = i;
    }

    // Platform Layer
    for (uint32_t i = 0; i < levelSize; i++) {
        uint32_t index = i + levelSize * 2;

        if (tmx->data[index] == TILE_ID_EMPTY) {
            // Is a blank tile, don't do anything
        }
        else {
            // Background tiles are non-solid. If semi-solidity (can jump up but not fall through) is required, use platforms (will be a separate layer).
            gameState.setMapTile(i % levelWidth, i / levelWidth, Tile::SOLID);
        }
    }


    // Reset player attributes
    gameState.player = Player(gameState.playerStartX, gameState.playerStartY, gameState.playerSelected);

    gameState.finish = Finish(finishX, finishY, finishFrames);

    gameState.checkpoint = Checkpoint(checkpointX, checkpointY);

    // Reset camera pos
    gameState.camera.pos.x = gameState.cameraStartX;
    gameState.camera.pos.y = gameState.cameraStartY;

    // Set player pos pointers
    for (uint8_t i = 0; i < gameState.enemies.size(); i++) {
        gameState.enemies[i].set_player_position(&gameState.player.pos.x, &gameState.player.pos.y);
    }

    // Check there aren't any levelTriggers which have levelNumber >= LEVEL_COUNT
    gameState.levelTriggers.erase(std::remove_if(gameState.levelTriggers.begin(), gameState.levelTriggers.end(), [](LevelTrigger& levelTrigger) { return levelTrigger.levelNumber >= LEVEL_COUNT; }), gameState.levelTriggers.end());
}
