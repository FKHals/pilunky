#include "GameInput.hpp"

#include "engine/input.hpp"

using namespace blit;

void ButtonStates::update() {
    if (buttons & Button::A) {
        if (A == 2) {
            A = 1;
        }
        else if (A == 0) {
            A = 2;
        }
    }
    else {
        A = 0;
    }

    if (buttons & Button::B) {
        if (B == 2) {
            B = 1;
        }
        else if (B == 0) {
            B = 2;
        }
    }
    else {
        B = 0;
    }

    if (buttons & Button::X) {
        if (X == 2) {
            X = 1;
        }
        else if (X == 0) {
            X = 2;
        }
    }
    else {
        X = 0;
    }

    if (buttons & Button::Y) {
        if (Y == 2) {
            Y = 1;
        }
        else if (Y == 0) {
            Y = 2;
        }
    }
    else {
        Y = 0;
    }

    if (buttons & Button::DPAD_UP) {
        if (UP == 2) {
            UP = 1;
        }
        else if (UP == 0) {
            UP = 2;
        }
    }
    else {
        UP = 0;
    }

    if (buttons & Button::DPAD_DOWN) {
        if (DOWN == 2) {
            DOWN = 1;
        }
        else if (DOWN == 0) {
            DOWN = 2;
        }
    }
    else {
        DOWN = 0;
    }

    if (buttons & Button::DPAD_LEFT) {
        if (LEFT == 2) {
            LEFT = 1;
        }
        else if (LEFT == 0) {
            LEFT = 2;
        }
    }
    else {
        LEFT = 0;
    }

    if (buttons & Button::DPAD_RIGHT) {
        if (RIGHT == 2) {
            RIGHT = 1;
        }
        else if (RIGHT == 0) {
            RIGHT = 2;
        }
    }
    else {
        RIGHT = 0;
    }
}